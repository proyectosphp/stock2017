-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-06-2017 a las 14:41:48
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `stock2017`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_ingreso`
--

CREATE TABLE IF NOT EXISTS `detalle_ingreso` (
  `deta_ingr_id` int(8) NOT NULL AUTO_INCREMENT,
  `deta_ingr_cant` float(8,2) NOT NULL,
  `ingr_id` int(8) NOT NULL,
  `prod_id` int(8) NOT NULL,
  PRIMARY KEY (`deta_ingr_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `detalle_ingreso`
--

INSERT INTO `detalle_ingreso` (`deta_ingr_id`, `deta_ingr_cant`, `ingr_id`, `prod_id`) VALUES
(1, 5.00, 1, 38),
(2, 50.00, 1, 5),
(3, 30.00, 7, 18),
(4, 50.00, 7, 3),
(5, 10.00, 7, 22),
(6, 5.00, 9, 189),
(7, 10.00, 9, 97),
(8, 5.00, 10, 189),
(16, 50.00, 15, 189),
(10, 5.00, 11, 189),
(11, 5.00, 12, 189);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido`
--

CREATE TABLE IF NOT EXISTS `detalle_pedido` (
  `deta_pedi_id` int(8) NOT NULL AUTO_INCREMENT,
  `deta_pedi_cant_pedi` float(8,2) NOT NULL,
  `deta_pedi_cant_acep` float(8,2) NOT NULL,
  `pedi_id` int(8) NOT NULL,
  `prod_id` int(8) NOT NULL,
  PRIMARY KEY (`deta_pedi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `detalle_pedido`
--

INSERT INTO `detalle_pedido` (`deta_pedi_id`, `deta_pedi_cant_pedi`, `deta_pedi_cant_acep`, `pedi_id`, `prod_id`) VALUES
(3, 5.00, 5.00, 3, 1),
(5, 4.00, 4.00, 3, 3),
(6, 7.00, 7.00, 3, 4),
(7, 9.99, 9.99, 4, 3),
(8, 2.00, 2.00, 4, 7),
(10, 9.99, 9.99, 5, 2),
(14, 9.99, 9.99, 8, 5),
(15, 9.99, 9.99, 8, 3),
(16, 9.99, 9.99, 8, 7),
(17, 9.99, 9.99, 7, 8),
(18, 9.99, 9.99, 7, 16),
(20, 3.00, 3.00, 7, 26),
(26, 3000.00, 3000.00, 28, 38),
(27, 1.00, 1.00, 28, 34),
(28, 50.00, 50.00, 32, 38);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_permiso`
--

CREATE TABLE IF NOT EXISTS `detalle_permiso` (
  `perm_deta_id` int(8) NOT NULL AUTO_INCREMENT,
  `luga_id` int(8) NOT NULL,
  `usua_id` int(8) NOT NULL,
  `tipo_perm_id` int(8) NOT NULL,
  PRIMARY KEY (`perm_deta_id`),
  KEY `luga_id` (`luga_id`),
  KEY `usua_id` (`usua_id`),
  KEY `tipo_perm_id` (`tipo_perm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `detalle_permiso`
--

INSERT INTO `detalle_permiso` (`perm_deta_id`, `luga_id`, `usua_id`, `tipo_perm_id`) VALUES
(6, 4, 3, 1),
(7, 5, 3, 2),
(8, 6, 3, 2),
(9, 7, 3, 2),
(10, 8, 3, 2),
(11, 9, 3, 2),
(12, 4, 4, 1),
(13, 5, 4, 2),
(14, 6, 4, 2),
(15, 7, 4, 2),
(16, 8, 4, 2),
(17, 9, 4, 2),
(18, 4, 5, 1),
(19, 5, 5, 2),
(20, 6, 5, 2),
(21, 7, 5, 2),
(22, 8, 5, 2),
(23, 9, 5, 2),
(24, 5, 6, 2),
(25, 8, 7, 2),
(26, 6, 8, 2),
(27, 7, 9, 2),
(28, 9, 10, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_pedido`
--

CREATE TABLE IF NOT EXISTS `estado_pedido` (
  `esta_pedi_id` int(8) NOT NULL AUTO_INCREMENT,
  `esta_pedi_nomb` varchar(50) NOT NULL,
  PRIMARY KEY (`esta_pedi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `estado_pedido`
--

INSERT INTO `estado_pedido` (`esta_pedi_id`, `esta_pedi_nomb`) VALUES
(1, 'Realizado'),
(2, 'Rechazado'),
(3, 'Aceptado'),
(4, 'Recibido'),
(5, 'En Proceso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso`
--

CREATE TABLE IF NOT EXISTS `ingreso` (
  `ingr_id` int(8) NOT NULL AUTO_INCREMENT,
  `ingr_nomb` varchar(50) NOT NULL,
  `ingr_fech` datetime NOT NULL,
  `usua_id` int(8) NOT NULL,
  `luga_id` int(8) NOT NULL,
  `ingr_ingre` varchar(50) NOT NULL,
  PRIMARY KEY (`ingr_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `ingreso`
--

INSERT INTO `ingreso` (`ingr_id`, `ingr_nomb`, `ingr_fech`, `usua_id`, `luga_id`, `ingr_ingre`) VALUES
(4, 'ingreso', '2016-11-01 01:54:39', 1, 3, 'No Ingresado'),
(3, 'ejemplo', '2016-10-21 23:07:25', 1, 3, 'No Ingresado'),
(5, 'deposito ', '2016-12-21 13:32:58', 1, 3, 'No Ingresado'),
(6, 'ejemplooo', '2017-05-29 19:32:48', 1, 3, 'No Ingresado'),
(7, 'Holita', '2017-06-01 21:57:29', 3, 9, 'Ingresado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

CREATE TABLE IF NOT EXISTS `lugar` (
  `luga_id` int(8) NOT NULL AUTO_INCREMENT,
  `luga_nomb` varchar(50) NOT NULL,
  `luga_depo` tinyint(1) NOT NULL,
  PRIMARY KEY (`luga_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `lugar`
--

INSERT INTO `lugar` (`luga_id`, `luga_nomb`, `luga_depo`) VALUES
(4, 'Admin', 0),
(5, 'Nueve43', 0),
(6, 'La Estaciòn', 0),
(7, 'Plaza Mayor', 0),
(8, 'Howard Johnson', 0),
(9, 'Deposito La Estaciòn', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE IF NOT EXISTS `pedido` (
  `pedi_id` int(8) NOT NULL AUTO_INCREMENT,
  `pedi_nomb` varchar(50) NOT NULL,
  `pedi_fech` datetime NOT NULL,
  `pedi_rest_fisi` tinyint(1) NOT NULL,
  `pedi_rest_disp` tinyint(1) NOT NULL,
  `pedi_sum_stock` tinyint(1) NOT NULL,
  `usua_id` int(8) NOT NULL,
  `luga_id` int(8) NOT NULL,
  `esta_pedi_id` int(8) NOT NULL,
  PRIMARY KEY (`pedi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`pedi_id`, `pedi_nomb`, `pedi_fech`, `pedi_rest_fisi`, `pedi_rest_disp`, `pedi_sum_stock`, `usua_id`, `luga_id`, `esta_pedi_id`) VALUES
(30, 'mjose', '2016-12-05 15:09:32', 0, 0, 0, 6, 5, 5),
(28, 'kevin', '2016-11-04 01:25:14', 0, 0, 0, 7, 8, 5),
(15, 'howard', '2016-10-20 16:41:12', 0, 0, 0, 7, 8, 5),
(14, 'pedido', '2016-10-17 11:28:24', 0, 0, 0, 3, 5, 3),
(31, 'Asd', '2017-05-29 20:04:32', 0, 0, 0, 3, 5, 5),
(32, 'Hollala', '2017-06-05 18:50:22', 0, 0, 0, 3, 5, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
  `prod_id` int(8) NOT NULL AUTO_INCREMENT,
  `prod_nomb` varchar(50) NOT NULL,
  `prod_cara` varchar(250) NOT NULL,
  `prod_cate_id` int(8) NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=231 ;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`prod_id`, `prod_nomb`, `prod_cara`, `prod_cate_id`) VALUES
(3, 'Carre ternera (kg)', 'kilo', 1),
(4, 'Matambre cerdo (kg)', 'kilo', 1),
(17, 'Matambre ternera (kg)', 'kilo', 1),
(18, 'Lomo', 'kilos', 1),
(19, 'Porc lomos', 'unidad', 1),
(20, 'Molida', 'kilo', 1),
(21, 'Tapa de nalga', 'unidad', 1),
(22, 'Bife de nalga', 'unidad', 1),
(23, 'Milanesa', 'unidad', 1),
(24, 'Nalga', 'unidades', 1),
(25, 'Milanesas 2x1', 'unidad', 1),
(30, 'Suprema', 'unidad', 1),
(32, 'Paleta', 'unidad', 4),
(33, 'Tybo', 'unidad', 4),
(34, 'J. Cocido', 'unidad', 4),
(35, 'J. Crudo', 'unidad', 4),
(36, 'Milan', 'unidad', 4),
(37, 'Panceta', 'unidad', 4),
(38, 'Bondiola', 'unidad', 4),
(39, 'Pategras', 'unidad', 4),
(40, 'Fontina', 'unidad', 4),
(41, 'Roquefor', 'unidad', 4),
(42, 'Mortadela', 'unidad', 4),
(45, 'Levadura', 'unidad', 4),
(46, 'Salamines', 'unidad', 4),
(47, 'Miga', 'unidad', 9),
(48, 'Pan de lomo', 'unidad', 9),
(49, 'Saborizados', 'unidad', 4),
(50, 'Manteca', 'unidad', 8),
(51, 'Casamcrem', 'unidad', 8),
(53, 'Coca', 'por unidad', 5),
(54, 'Sprite ', 'por unidad', 5),
(55, 'Coca Light ', 'por unidad', 5),
(56, 'Coca Zero', 'por unidad', 5),
(57, 'Sprite zero', 'por unidad ', 5),
(58, 'Fanta ', 'por unidad', 5),
(59, 'Agua sin gas ', 'por unidad', 5),
(60, 'Agua con gas', 'por unidad', 5),
(61, 'Schwep tonica', 'por unidad', 5),
(62, 'schwep pomelo', 'por unidad', 5),
(63, 'Aquarius  Pomelo', 'por unidad', 5),
(64, 'Aquarius Manzana', 'por unidad', 5),
(65, 'Aquarius Pera', 'por unidad', 5),
(66, 'Aquarius Uva', 'por unidad', 5),
(67, 'aquarius Naranja', 'por unidad', 5),
(68, 'Aquarius Limonada', 'por unidad', 5),
(69, 'Budweiser 1lt', 'por unidad', 6),
(70, 'Budweiser 470ml', 'por unidad', 6),
(71, 'Budweiser 330', 'por unidad', 6),
(72, 'Heineken 470', 'por unidad', 6),
(73, 'Heineken 1lts', 'por unidad', 6),
(74, 'Heineken 330', 'por unidad', 6),
(75, 'Imperial lager 470ml', 'por unidad', 6),
(76, 'Imperial Lager 1lt', 'por unidad', 6),
(77, 'Imperial scotch Ale 330', 'por unidad', 6),
(78, 'imperial scotch Ale 1lt', 'por unidad', 6),
(79, 'Imperial Amber Lager 330', 'por unidad', 6),
(80, 'Imperial Amber lager 1lt', 'por unidad', 6),
(81, 'Imperial cream Stouch 330', 'por unidad', 6),
(82, 'Imperial cream Stouch 1lt', 'por unidad', 6),
(83, 'Imperial trigo 330 ', 'por unidad', 6),
(84, 'Imperial trigo 1lt', 'por unidad', 6),
(85, 'Mejillones', 'kilo', 7),
(86, 'Langostinos', 'kilo', 7),
(87, 'callo vieras', 'kilo', 7),
(88, 'Tentaculos', 'kilo', 7),
(89, 'Tubos de calamar', 'kilo', 7),
(90, 'Rabas', 'unidad', 7),
(91, 'Abadejo', 'unidad', 7),
(92, 'Merluza ', 'kilo', 7),
(93, 'Pencas de Salmon', 'kilo', 7),
(94, 'Salmon', 'unidad', 7),
(95, 'Anicama', 'kilos', 7),
(96, 'Camarones/Langostino', 'kilos', 7),
(97, 'Truchas', 'unidad', 7),
(98, 'Berberechos', 'kilos', 7),
(99, 'Almejas', 'kilos', 7),
(100, 'Media Valva', 'kilos', 7),
(101, 'Crema', 'kilos', 8),
(102, 'Queso Mantecos', 'unidad', 4),
(103, 'Queso Crema', 'unidad', 8),
(104, 'Baguette', 'unidad', 9),
(105, 'Rebozador', 'kilos', 9),
(106, 'Provoletas', 'unidad', 4),
(107, 'Levaduras', 'unidad', 4),
(108, 'bife de chorizo', 'unidad', 1),
(109, 'Pollo ', 'unidad', 1),
(110, 'Pechuga', 'unidad', 1),
(111, 'Muslo', 'unidad', 1),
(112, 'Pollo Relleno', 'unidad', 1),
(113, 'Bondiola de cerdo', 'unidad', 1),
(114, 'Solomillo de cerdo', 'unidad', 1),
(115, 'carre de cerdo', 'unidad', 1),
(116, 'choclo', 'unidad', 10),
(117, 'champignones', 'unidad', 10),
(118, 'Arvejas', 'unidad', 10),
(119, 'Anana', 'unidad', 10),
(120, 'Durazno', 'u', 0),
(121, 'Durazno en lata', 'unidad', 10),
(122, 'Pure de tomate', 'unidad', 10),
(123, 'Tomate Lata', 'unidad', 10),
(124, 'Sala de soja', 'unidad', 10),
(125, 'Miel', 'unidad', 10),
(126, 'Azucar', 'kilo', 10),
(127, 'Sal Fina', 'unidad', 10),
(128, 'Sal Gruesa', 'unidad', 10),
(129, 'Vinagre', 'unidad', 10),
(130, 'Vino tinto', 'unidad', 10),
(131, 'Vino blanco', 'unidad', 10),
(132, 'Aceituna Verde', 'balde', 10),
(133, 'Aceituna Negras', 'balde', 10),
(134, 'Arroz', 'kilo', 10),
(135, 'Mayonesa 2.900', 'unidad', 10),
(136, 'Mostaza 2.900', 'unidad', 10),
(137, 'Harina', 'kilos', 10),
(138, 'Maicena', 'kilos', 10),
(139, 'Aceite de Oliva', 'litro', 10),
(140, 'Aceite', 'litro', 10),
(141, 'mayonesa Mini', 'caja', 10),
(142, 'Mostaza Mini', 'caja', 10),
(143, 'Kepchut Mini', 'caja', 10),
(144, 'Aceto', 'litro', 10),
(145, 'Atun', 'unidad', 10),
(146, 'Pizzas', 'unidad', 9),
(147, 'cazuela Marisco', 'unidad', 7),
(148, 'Tabla de Mar', 'unidad', 7),
(149, 'Tabla de Fiambre', 'unidad', 4),
(150, 'Pan Lactal Blanco', 'unidad', 9),
(151, 'Pan Lactal negro', 'unidad', 9),
(152, 'Budin Ingles', 'unidad', 9),
(153, 'Pasta frola grande', 'unidad', 9),
(154, 'Pasta Frola chica', 'unidad', 9),
(155, 'Alfajores de maicena', 'unidad', 9),
(156, 'Pepas', 'kilo', 9),
(157, 'scones', 'kilo', 9),
(158, 'Tiramisu', 'unidad', 9),
(159, 'Brownie', 'unidad', 9),
(160, 'Chesse Cake', 'unidad', 9),
(161, 'lemon pie', 'unidad', 9),
(162, 'Selva Negra', 'unidad', 9),
(163, 'Tarta especial', 'unidad', 9),
(164, 'Cafe Molido', 'kilo', 11),
(165, 'Cafe grano', 'kilo', 11),
(166, 'Azucar mini', 'caja', 11),
(167, 'Edulcorante', 'caja', 11),
(168, 'Mermelada Mini', 'caja', 11),
(169, 'Manteca Mini', 'caja', 11),
(170, 'Queso Untable', 'caja', 11),
(171, 'dulce de leche Mini', 'caja', 11),
(172, 'dulce de leche', 'kilo', 8),
(173, 'yogurt frutilla entero', 'litro', 8),
(174, 'yogurt vainilla entero', 'litro', 8),
(175, 'yogurt frutilla light ', 'litro ', 8),
(176, 'yogurt vainilla light', 'litro', 8),
(177, 'Jugo Citric', 'litro', 11),
(178, 'Jugo Cepita', 'litro', 11),
(179, 'Medialunas', 'unidad', 11),
(180, 'Criollos', 'unidad', 11),
(181, 'Chocolino', 'kilo', 11),
(182, 'Chocolate en Barra', 'Kilo', 11),
(183, 'Leche', 'litro', 8),
(184, 'Te comun', 'caja', 11),
(185, 'Te hierbas', 'caja', 11),
(186, 'Te Boldo', 'caja', 11),
(187, 'Te Saborizados', 'caja', 11),
(188, 'Leche Sobre Individual', 'caja', 11),
(189, 'Peras', 'kilo', 12),
(190, 'Manzanas', 'Kilos', 12),
(191, 'kiwi', 'kilos', 12),
(192, 'Frutilla', 'kilo', 12),
(193, 'Naranjas', 'kilos', 12),
(194, 'Tomates', 'kilo', 12),
(195, 'Limones', 'unidades', 12),
(196, 'Batatas', 'kilos', 12),
(197, 'Zanahoria', 'kilo', 12),
(198, 'Papas', 'bolsa', 12),
(199, 'Verdeo', 'unidad', 12),
(200, 'Perejil', 'atado', 12),
(201, 'champignones frescos', 'kilo', 12),
(202, 'Tomate Cherry', 'kilo', 12),
(203, 'Rucula', 'atado', 12),
(204, 'Lechuga', 'kilo', 12),
(205, 'Coliflor', 'unidad', 12),
(206, 'Brocoli', 'unidad', 12),
(207, 'Anco', 'unidad', 12),
(208, 'Zapallitos', 'kilo', 12),
(209, 'Berenjena', 'kilo', 12),
(210, 'Pimiento Verdes', 'kilo', 12),
(211, 'Pimiento Rojo', 'kilo', 12),
(212, 'Pimiento Amarillo', 'kilo', 12),
(213, 'Pepinos', 'unidad', 12),
(214, 'Paltas', 'unidad', 12),
(215, 'Papas Baton', 'bolsa', 12),
(216, 'Papas Noisette ', 'bolsa', 12),
(217, 'Acelga', 'maso', 12),
(218, 'Cebolla', 'kilo', 12),
(219, 'Huevos', 'maple', 12),
(220, 'Sorrentino Calabaza', 'porciones', 13),
(221, 'Sorrentinos Jamon y queso', 'porciones', 13),
(222, 'Ravioles de Lomo y Hongos', 'porciones', 13),
(223, 'Sorrentinos Capresse', 'porciones', 13),
(224, 'Ravioles Perla Negra', 'porciones', 13),
(225, 'Ravioles Pollo y Verdura', 'porciones', 13),
(226, 'Lasagna', 'porciones', 13),
(227, 'Canelones', 'porciones', 13),
(228, 'Sorrentino de camarones', 'porciones', 13),
(229, 'Tallarines', 'kilo', 13),
(230, 'Noquis', 'porciones', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_categoria`
--

CREATE TABLE IF NOT EXISTS `producto_categoria` (
  `prod_cate_id` int(8) NOT NULL AUTO_INCREMENT,
  `prod_cate_nomb` varchar(50) NOT NULL,
  PRIMARY KEY (`prod_cate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `producto_categoria`
--

INSERT INTO `producto_categoria` (`prod_cate_id`, `prod_cate_nomb`) VALUES
(1, 'Carne'),
(4, 'Fiambre'),
(5, 'BEBIDAS SIN ALCOHOL'),
(6, 'BEBIDAS CON ALCOHOL'),
(7, 'Pescado'),
(8, 'Lacteos'),
(9, 'Panificacion'),
(10, 'Almacen'),
(11, 'Cafeteria'),
(12, 'Verduleria'),
(13, 'Pastas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `stoc_id` int(8) NOT NULL AUTO_INCREMENT,
  `stoc_cant_disp` float(8,2) NOT NULL,
  `stoc_cant_fisi` float(8,2) NOT NULL,
  `stoc_fech_modi` datetime NOT NULL,
  `prod_id` int(8) NOT NULL,
  `luga_id` int(8) NOT NULL,
  PRIMARY KEY (`stoc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=250 ;

--
-- Volcado de datos para la tabla `stock`
--

INSERT INTO `stock` (`stoc_id`, `stoc_cant_disp`, `stoc_cant_fisi`, `stoc_fech_modi`, `prod_id`, `luga_id`) VALUES
(41, 0.00, 15.00, '2016-11-22 19:35:16', 38, 8),
(40, 0.00, 3.00, '2016-11-22 19:35:40', 34, 8),
(39, 0.00, 3.00, '2016-11-22 19:35:57', 35, 8),
(38, 0.00, 5.00, '2016-11-22 19:36:21', 97, 8),
(37, 0.00, 1.00, '2016-11-22 19:36:31', 95, 8),
(36, 0.00, 15.00, '2016-11-22 19:36:50', 94, 8),
(35, 0.00, 22.00, '2016-11-22 19:37:12', 90, 8),
(31, 0.00, 20.00, '2016-10-21 21:27:29', 23, 7),
(236, 0.00, 50.00, '2017-06-05 18:31:27', 189, 9),
(242, 0.00, 24.00, '2016-12-21 13:51:26', 128, 9),
(33, 0.00, 11.50, '2016-11-22 19:38:21', 86, 8),
(32, 0.00, 1.00, '2016-11-22 19:38:31', 85, 8),
(42, 0.00, 0.00, '2016-11-22 19:38:41', 41, 8),
(56, 0.00, 2.00, '2017-01-18 18:13:54', 3, 5),
(43, 0.00, 0.00, '2016-11-12 19:41:45', 39, 8),
(241, 0.00, 10.00, '2016-12-21 13:50:02', 126, 9),
(45, 0.00, 0.50, '2016-11-22 19:39:42', 40, 8),
(46, 0.00, 1.00, '2016-11-22 19:39:53', 42, 8),
(47, 0.00, 10.00, '2016-11-22 19:40:06', 101, 8),
(48, 0.00, 7.00, '2016-11-22 19:40:57', 32, 8),
(49, 0.00, 1.00, '2016-11-22 19:41:10', 33, 8),
(50, 0.00, 1.00, '2016-11-22 19:41:23', 102, 8),
(51, 0.00, 1.05, '2016-11-22 19:41:55', 36, 8),
(53, 0.00, 16.00, '2016-11-22 19:42:22', 103, 8),
(54, 0.00, 11.00, '2016-11-22 19:43:04', 37, 8),
(55, 0.00, 2.00, '2016-11-22 19:45:46', 46, 8),
(57, 0.00, 1.00, '2017-01-18 18:14:22', 4, 5),
(58, 0.00, 4.00, '2017-01-18 18:14:43', 17, 5),
(59, 0.00, 3.00, '2016-10-26 02:36:59', 18, 5),
(60, 0.00, 2.00, '2017-01-18 18:15:04', 19, 5),
(61, 0.00, 0.00, '2016-10-25 18:15:35', 20, 5),
(62, 0.00, 0.00, '2016-10-25 18:15:53', 21, 5),
(63, 0.00, 0.00, '2016-10-25 18:16:15', 22, 5),
(64, 0.00, 0.00, '2016-10-25 18:16:24', 25, 5),
(65, 0.00, 7.00, '2017-01-18 18:16:20', 30, 5),
(66, 0.00, 2.00, '2016-10-26 02:26:33', 32, 5),
(67, 0.00, 1.00, '2016-10-26 02:27:01', 33, 5),
(68, 0.00, 0.00, '2017-01-18 18:16:39', 34, 5),
(70, 0.00, 1.00, '2016-10-26 02:27:50', 35, 5),
(71, 0.00, 0.00, '2016-10-26 02:28:45', 36, 5),
(72, 0.00, 1.00, '2016-10-26 02:29:39', 37, 5),
(73, 0.00, 3.00, '2016-10-26 02:31:06', 38, 5),
(74, 0.00, 2.00, '2016-10-26 02:31:25', 39, 5),
(75, 0.00, 0.00, '2016-10-25 18:30:33', 40, 5),
(76, 0.00, 1.00, '2016-10-26 02:32:53', 41, 5),
(77, 0.00, 0.00, '2016-10-25 18:31:34', 42, 5),
(78, 0.00, 2.00, '2016-10-26 02:33:38', 46, 5),
(79, 0.00, 1.00, '2016-10-26 02:33:51', 47, 5),
(80, 0.00, 22.00, '2016-10-26 02:34:10', 48, 5),
(81, 0.00, 3.00, '2016-10-26 02:34:24', 49, 5),
(82, 0.00, 0.00, '2016-10-25 19:26:32', 50, 5),
(83, 0.00, 1.00, '2016-10-26 02:35:03', 51, 5),
(84, 0.00, 15.00, '2016-10-26 02:35:34', 85, 5),
(85, 0.00, 1.00, '2016-10-26 02:35:51', 86, 5),
(86, 0.00, 0.00, '2016-10-25 19:28:05', 87, 5),
(87, 0.00, 2.00, '2016-10-26 02:38:26', 88, 5),
(88, 0.00, 0.00, '2016-10-25 19:28:59', 89, 5),
(89, 0.00, 15.00, '2016-10-26 02:39:05', 90, 5),
(90, 0.00, 0.00, '2016-10-25 19:30:03', 91, 5),
(91, 0.00, 4.00, '2016-10-26 02:39:31', 92, 5),
(92, 0.00, 8.00, '2016-10-26 02:41:08', 93, 5),
(93, 0.00, 0.00, '2016-10-25 19:32:45', 94, 5),
(94, 0.00, 0.00, '2016-10-25 19:33:04', 95, 5),
(95, 0.00, 0.00, '2016-10-25 19:33:26', 96, 5),
(96, 0.00, 12.00, '2016-10-26 02:41:57', 97, 5),
(97, 0.00, 0.00, '2016-10-25 19:33:59', 98, 5),
(98, 0.00, 0.00, '2016-10-25 19:34:16', 99, 5),
(99, 0.00, 0.00, '2016-10-25 19:34:31', 100, 5),
(100, 0.00, 0.00, '2016-10-26 02:42:36', 101, 5),
(101, 0.00, 0.00, '2016-10-25 19:35:28', 102, 5),
(102, 0.00, 0.00, '2016-10-25 19:35:45', 104, 5),
(103, 0.00, 0.00, '2016-10-25 19:35:46', 104, 5),
(104, 0.00, 1.00, '2016-10-26 02:43:01', 105, 5),
(105, 0.00, 0.00, '2016-10-25 19:36:46', 106, 5),
(106, 0.00, 5.00, '2016-10-26 02:45:07', 109, 5),
(107, 0.00, 15.00, '2016-10-26 02:47:15', 110, 5),
(108, 0.00, 11.00, '2016-10-26 02:47:30', 111, 5),
(109, 0.00, 0.00, '2016-10-25 19:38:07', 112, 5),
(110, 0.00, 3.00, '2016-10-26 02:48:06', 38, 5),
(111, 0.00, 0.00, '2016-10-25 19:38:35', 114, 5),
(112, 0.00, 0.00, '2016-10-25 19:38:51', 115, 5),
(113, 0.00, 6.00, '2016-10-26 02:48:39', 116, 5),
(114, 0.00, 6.00, '2016-10-26 02:49:16', 117, 5),
(115, 0.00, 4.00, '2016-10-26 02:49:52', 119, 5),
(116, 0.00, 5.00, '2016-10-26 02:50:10', 121, 5),
(117, 0.00, 12.00, '2016-10-26 02:51:16', 122, 5),
(118, 0.00, 2.00, '2016-10-26 02:51:35', 123, 5),
(119, 0.00, 3.00, '2016-10-26 02:51:58', 124, 5),
(120, 0.00, 3.00, '2016-10-26 02:52:26', 125, 5),
(121, 0.00, 10.00, '2016-11-01 01:25:53', 126, 5),
(122, 0.00, 4.00, '2016-10-26 02:53:47', 127, 5),
(123, 0.00, 2.00, '2016-10-26 02:54:05', 128, 5),
(124, 0.00, 2.00, '2016-10-26 02:54:24', 129, 5),
(125, 0.00, 4.00, '2016-10-26 02:54:47', 130, 5),
(126, 0.00, 2.00, '2016-10-26 02:55:16', 131, 5),
(127, 0.00, 0.00, '2016-10-25 19:53:22', 132, 5),
(128, 0.00, 1.00, '2016-10-26 02:56:21', 133, 5),
(129, 0.00, 0.00, '2016-10-25 19:54:52', 134, 5),
(130, 0.00, 1.00, '2016-10-26 02:57:25', 135, 5),
(131, 0.00, 10.00, '2016-10-28 15:19:59', 137, 5),
(132, 0.00, 5.00, '2016-10-28 15:21:05', 138, 5),
(135, 0.00, 1.00, '2016-11-22 20:01:46', 3, 8),
(137, 0.00, 8.00, '2016-11-22 20:02:48', 50, 8),
(139, 0.00, 2.00, '2016-11-18 20:35:24', 47, 8),
(231, 0.00, 1.00, '2016-11-22 20:04:05', 104, 8),
(141, 0.00, 12.00, '2016-11-22 20:04:51', 48, 8),
(142, 0.00, 8.00, '2016-11-22 20:05:11', 106, 8),
(143, 0.00, 2.00, '2016-11-22 20:05:36', 216, 8),
(144, 0.00, 1.05, '2016-11-22 20:07:58', 45, 8),
(147, 0.00, 1.00, '2016-11-22 20:08:38', 189, 8),
(148, 0.00, 0.00, '2016-11-22 20:08:52', 190, 8),
(149, 0.00, 1.00, '2016-11-22 20:09:45', 191, 8),
(150, 0.00, 2.00, '2016-11-22 20:09:57', 193, 8),
(151, 0.00, 3.00, '2016-11-18 20:37:59', 194, 8),
(152, 0.00, 3.00, '2016-11-22 20:10:11', 195, 8),
(153, 0.00, 1.50, '2016-11-22 20:10:29', 196, 8),
(154, 0.00, 3.00, '2016-11-22 20:10:41', 197, 8),
(155, 0.00, 0.00, '2016-11-22 20:11:38', 198, 8),
(156, 0.00, 0.00, '2016-11-22 20:11:56', 199, 8),
(157, 0.00, 0.50, '2016-11-22 20:12:13', 200, 8),
(158, 0.00, 200.00, '2016-11-22 20:12:33', 201, 8),
(159, 0.00, 1.50, '2016-11-22 20:12:52', 202, 8),
(160, 0.00, 4.00, '2016-11-22 20:13:14', 203, 8),
(161, 0.00, 6.00, '2016-11-22 20:13:28', 204, 8),
(244, 0.00, 67.00, '2017-01-18 18:15:46', 23, 5),
(163, 0.00, 0.00, '2016-11-22 20:17:31', 205, 8),
(239, 0.00, 3.00, '2016-12-21 13:47:37', 191, 9),
(165, 0.00, 0.00, '2016-11-22 20:20:58', 207, 8),
(166, 0.00, 5.00, '2016-11-22 20:21:16', 208, 8),
(167, 0.00, 3.00, '2016-11-22 20:21:55', 209, 8),
(168, 0.00, 3.00, '2016-11-12 20:08:29', 210, 8),
(169, 0.00, 2.00, '2016-11-22 20:22:17', 211, 8),
(170, 0.00, 4.00, '2016-11-22 20:22:30', 213, 8),
(171, 0.00, 0.00, '2016-11-03 08:12:36', 214, 8),
(172, 0.00, 0.00, '2016-11-03 08:13:06', 212, 8),
(173, 0.00, 6.00, '2016-11-22 20:22:53', 215, 8),
(243, 0.00, 34.00, '2016-12-21 13:57:30', 183, 9),
(175, 0.00, 0.00, '2016-11-03 08:29:19', 217, 8),
(176, 0.00, 4.00, '2016-11-22 20:23:14', 218, 8),
(177, 0.00, 1.00, '2016-11-22 20:23:33', 18, 8),
(178, 0.00, 0.00, '2016-11-22 20:26:19', 24, 8),
(179, 0.00, 90.00, '2016-11-22 20:34:12', 24, 8),
(180, 0.00, 16.00, '2016-11-22 20:41:30', 109, 8),
(181, 0.00, 26.00, '2016-11-22 20:41:56', 110, 8),
(182, 0.00, 50.00, '2016-11-22 20:42:13', 111, 8),
(183, 0.00, 2.00, '2016-11-22 20:42:26', 112, 8),
(185, 0.00, 1.00, '2016-11-22 20:42:40', 4, 8),
(186, 0.00, 1.00, '2016-11-22 20:42:55', 113, 8),
(187, 0.00, 1.50, '2016-11-22 20:43:26', 115, 8),
(188, 0.00, 5.00, '2016-11-22 20:43:58', 116, 8),
(189, 0.00, 3.00, '2016-11-22 20:44:20', 117, 8),
(190, 0.00, 17.00, '2016-11-22 20:44:38', 118, 8),
(238, 0.00, 24.00, '2016-12-21 13:42:53', 116, 9),
(192, 0.00, 4.00, '2016-11-22 21:10:26', 119, 8),
(193, 0.00, 3.00, '2016-11-22 21:11:28', 121, 8),
(198, 0.00, 2.00, '2016-11-08 22:35:38', 124, 8),
(195, 0.00, 10.00, '2016-11-22 21:12:14', 122, 8),
(196, 0.00, 1.00, '2016-11-22 21:15:07', 123, 8),
(200, 0.00, 6.00, '2016-11-22 21:16:07', 126, 8),
(201, 0.00, 5.00, '2016-11-22 21:16:46', 127, 8),
(202, 0.00, 4.00, '2016-11-22 21:16:59', 128, 8),
(203, 0.00, 2.00, '2016-11-18 20:51:11', 129, 8),
(208, 0.00, 3.00, '2016-11-22 21:17:22', 130, 8),
(209, 0.00, 3.00, '2016-11-22 21:17:35', 131, 8),
(211, 0.00, 1.05, '2016-11-22 21:18:33', 132, 8),
(212, 0.00, 0.50, '2016-11-22 21:18:49', 133, 8),
(233, 0.00, 2.05, '2016-11-22 21:19:09', 105, 8),
(214, 0.00, 7.00, '2016-11-22 21:19:25', 134, 8),
(215, 0.00, 1.00, '2016-11-22 21:19:46', 18, 8),
(237, 0.00, 2.00, '2016-12-21 13:40:46', 190, 9),
(217, 0.00, 2.00, '2016-11-22 21:20:05', 135, 8),
(218, 0.00, 1.00, '2016-11-22 21:20:16', 136, 8),
(219, 0.00, 9.00, '2016-11-22 21:21:33', 219, 8),
(220, 0.00, 1.05, '2016-11-18 20:53:11', 137, 8),
(221, 0.00, 1.00, '2016-11-22 21:22:04', 138, 8),
(222, 0.00, 5.00, '2016-11-22 21:22:19', 139, 8),
(223, 0.00, 30.00, '2016-11-22 21:22:39', 140, 8),
(224, 0.00, 2.00, '2016-11-18 20:54:22', 144, 8),
(225, 0.00, 11.00, '2016-11-18 20:54:37', 145, 8),
(226, 0.00, 22.00, '2016-11-22 21:22:57', 89, 8),
(227, 0.00, 0.00, '2016-11-04 06:42:11', 91, 8),
(228, 0.00, 2.00, '2016-11-22 21:23:10', 92, 8),
(234, 0.00, 5.00, '2016-11-12 20:44:39', 88, 8),
(230, 0.00, 1.00, '2016-11-22 21:23:26', 113, 8),
(232, 0.00, 0.00, '2016-11-22 21:23:43', 206, 8),
(245, 0.00, 30.00, '2017-06-05 18:14:10', 18, 9),
(246, 0.00, 50.00, '2017-06-05 18:14:10', 3, 9),
(247, 0.00, 10.00, '2017-06-05 18:14:10', 22, 9),
(248, 0.00, 60.00, '2017-06-05 18:30:16', 97, 9),
(249, 0.00, 10.00, '2017-06-05 18:30:16', 36, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_permiso`
--

CREATE TABLE IF NOT EXISTS `tipo_permiso` (
  `tipo_perm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_perm_nomb` varchar(50) NOT NULL,
  PRIMARY KEY (`tipo_perm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipo_permiso`
--

INSERT INTO `tipo_permiso` (`tipo_perm_id`, `tipo_perm_nomb`) VALUES
(1, 'admin'),
(2, 'encargado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usua_id` int(8) NOT NULL AUTO_INCREMENT,
  `usua_nomb` varchar(50) NOT NULL,
  `usua_pass` varchar(8) NOT NULL,
  PRIMARY KEY (`usua_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usua_id`, `usua_nomb`, `usua_pass`) VALUES
(10, 'deposito', '123456'),
(3, 'mjose', '123456'),
(4, 'leo', '123456'),
(5, 'claudio', '123456'),
(6, 'nueve43', '123456'),
(7, 'howard', '123456'),
(8, 'estacion', '123456'),
(9, 'plaza', '123456');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
