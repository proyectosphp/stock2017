<?php
include_once ('clases/stock-Modelo.php');
class stockControlador{
    //atributos
    private $stock;

public function __construct() {
    $this->stock = new stock();
    }

public function index($luga_id){
		$this->stock->set("luga_id", $luga_id);
        $resultado=$this->stock->listar();  
        return$resultado;
    }

public function crear($stoc_cant_fisi, $stoc_fech_modi, $prod_id, $luga_id){
    $this->stock->set("stoc_cant_fisi", $stoc_cant_fisi);
        $this->stock->set("stoc_fech_modi", $stoc_fech_modi);
        $this->stock->set("prod_id", $prod_id);
        $this->stock->set("luga_id", $luga_id);
        
    $resultado = $this->stock->crear();
        return $resultado;

    }
public function editar($stoc_id, $stoc_cant_fisi, $stoc_fech_modi, $prod_id, $luga_id){
    $this->stock->set("stoc_id", $stoc_id);
        $this->stock->set("stoc_cant_fisi", $stoc_cant_fisi);
        $this->stock->set("stoc_fech_modi", $stoc_fech_modi);
        $this->stock->set("prod_id", $prod_id);
        $this->stock->set("luga_id", $luga_id);
        $this->stock->editar();

}

public function editarprocess($stoc_id, $stoc_cant_fisi){
    $this->stock->set("stoc_id", $stoc_id);
        $this->stock->set("stoc_cant_fisi", $stoc_cant_fisi);
        $this->stock->editarprocess();

}

    public function eliminar($id){
        $this->stock->set("id", $id);
        $this->stock->eliminar();
    }
    

    public function ver(){
        $datos = $this->stock->ver();
        return $datos;
    }

}
