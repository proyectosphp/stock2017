<?php
include_once ('clases/producto_categoria-Modelo.php');
class producto_categoriaControlador{
    //atributos
    private $producto_categoria;

public function __construct() {
    $this->producto_categoria = new producto_categoria();
    }

public function index(){
        $resultado=$this->producto_categoria->listar();  
        return$resultado;
    }

public function crear($prod_cate_nomb){
    $this->producto_categoria->set("prod_cate_nomb", $prod_cate_nomb);
        
    $resultado = $this->producto_categoria->crear();
        return $resultado;

    }
public function editar($prod_cate_id, $prod_cate_nomb){
    $this->producto_categoria->set("prod_cate_id", $prod_cate_id);
        $this->producto_categoria->set("prod_cate_nomb", $prod_cate_nomb);
        $this->producto_categoria->editar();

}

    public function eliminar($id){
        $this->producto_categoria->set("id", $id);
        $this->producto_categoria->eliminar();
    }
    

    public function ver(){
        $datos = $this->producto_categoria->ver();
        return $datos;
    }

}
