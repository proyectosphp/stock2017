<?php


include_once('clases/informacion.php');

class controladorInformacion{
    //atributos
    private $informacion;
    
    //metodos
    public function __construct() {
        $this->informacion = new informacion();
    }
    public function index(){
        $resultado = $this->informacion->detalleStock();  
        return $resultado;
    }
    
    public function productosStock(){
        $resultado = $this->informacion->productosStock();  
        return $resultado;
    }
    
    public function todoStock(){
        $resultado = $this->informacion->todoStock();  
        return $resultado;
    }
    
    public function sumaProducto($idProoducto){
        $this->informacion->set("idProoducto", $idProoducto);
        $resultado = $this->informacion->sumaProducto();  
        return $resultado;
    }
    
}
?>