<?php
include_once ('clases/detalle_pedido-Modelo.php');
class detalle_pedidoControlador{
    //atributos
    private $detalle_pedido;

public function __construct() {
    $this->detalle_pedido = new detalle_pedido();
    }

public function index($pedi_id, $luga_id){
        $this->detalle_pedido->set("pedi_id", $pedi_id);
        $this->detalle_pedido->set("luga_id", $luga_id);
        $resultado=$this->detalle_pedido->listar();  
        return$resultado;
    }

public function indexstock($luga_id, $prod_id){
        $this->detalle_pedido->set("luga_id", $luga_id);
        $this->detalle_pedido->set("prod_id", $prod_id);
        $resultado=$this->detalle_pedido->listarstock();  
        return$resultado;
    }

public function crear($deta_pedi_cant_pedi, $deta_pedi_cant_acep, $pedi_id, $prod_id){
    $this->detalle_pedido->set("deta_pedi_cant_pedi", $deta_pedi_cant_pedi);
        $this->detalle_pedido->set("deta_pedi_cant_acep", $deta_pedi_cant_acep);
        $this->detalle_pedido->set("pedi_id", $pedi_id);
        $this->detalle_pedido->set("prod_id", $prod_id);
        
    $resultado = $this->detalle_pedido->crear();
        return $resultado;

    }
public function editar($deta_pedi_id, $deta_pedi_cant_pedi, $deta_pedi_cant_acep, $prod_id){
    $this->detalle_pedido->set("deta_pedi_id", $deta_pedi_id);
        $this->detalle_pedido->set("deta_pedi_cant_pedi", $deta_pedi_cant_pedi);
        $this->detalle_pedido->set("deta_pedi_cant_acep", $deta_pedi_cant_acep);
        $this->detalle_pedido->set("prod_id", $prod_id);
        $this->detalle_pedido->editar();

}

public function editarrealizado($pedi_id){
        $this->detalle_pedido->set("pedi_id", $pedi_id);
        $this->detalle_pedido->editarrealizado();

}

public function editaraceptado($pedi_id){
        $this->detalle_pedido->set("pedi_id", $pedi_id);
        $this->detalle_pedido->editaraceptado();

}

public function editarrecibido($pedi_id){
        $this->detalle_pedido->set("pedi_id", $pedi_id);
        $this->detalle_pedido->editarrecibido();

}

public function editarrechazado($pedi_id){
        $this->detalle_pedido->set("pedi_id", $pedi_id);
        $this->detalle_pedido->editarrechazado();

}

public function editardepo($deta_pedi_id, $deta_pedi_cant_acep){
    $this->detalle_pedido->set("deta_pedi_id", $deta_pedi_id);
        $this->detalle_pedido->set("deta_pedi_cant_acep", $deta_pedi_cant_acep);
        $this->detalle_pedido->editardepo();

}

    public function eliminar($id){
        $this->detalle_pedido->set("id", $id);
        $this->detalle_pedido->eliminar();
    }
    

    public function ver($prod_id, $luga_id){
        $this->detalle_pedido->set("prod_id", $prod_id);
        $this->detalle_pedido->set("luga_id", $luga_id);
        $datos = $this->detalle_pedido->ver();
        return $datos;
    }

}
