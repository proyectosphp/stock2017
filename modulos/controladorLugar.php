<?php


include_once('clases/lugar.php');

class controladorLugar{
    //atributos
    private $lugar;
    
    //metodos
    public function __construct() {
        $this->lugar = new lugar();
    }
    public function index(){
        $resultado = $this->lugar->listar();  
        return $resultado;
    }
    public function crear($nombre, $depo){
        $this->lugar->set("nombre", $nombre);
        $this->lugar->set("depo", $depo);
        
        $resultado = $this->lugar->crear();
        return $resultado;
    }
    public function eliminar($id){
        $this->lugar->set("id", $id);
        $this->lugar->eliminar();
    }
    public function ver($id){
        $this->lugar->set("id", $id);
        $datos = $this->lugar->ver();
        return $datos;
    }
    public function editar($id, $nombre, $depo){
        $this->lugar->set("id", $id);
        $this->lugar->set("nombre", $nombre);
        $this->lugar->set("depo", $depo);
        $this->lugar->editar();
    }
    
    
}

?>