<?php
include_once ('clases/ingreso-Modelo.php');
class ingresoControlador{
    //atributos
    private $ingreso;

public function __construct() {
    $this->ingreso = new ingreso();
    }

public function index($fechaini, $fechafin){
        $this->ingreso->set("fechaini", $fechaini);
        $this->ingreso->set("fechafin", $fechafin);
        $resultado=$this->ingreso->listar();  
        return$resultado;
    }

public function crear($ingr_nomb, $ingr_fech, $usua_id, $luga_id){
    $this->ingreso->set("ingr_nomb", $ingr_nomb);
        $this->ingreso->set("ingr_fech", $ingr_fech);
        $this->ingreso->set("usua_id", $usua_id);
        $this->ingreso->set("luga_id", $luga_id);
        
    $resultado = $this->ingreso->crear();
        return $resultado;

    }
public function editar($ingr_id, $ingr_nomb){
    $this->ingreso->set("ingr_id", $ingr_id);
        $this->ingreso->set("ingr_nomb", $ingr_nomb);
        $this->ingreso->editar();

}

public function editarestado($ingr_id){
    $this->ingreso->set("ingr_id", $ingr_id);
        $this->ingreso->editarestado();

}

    public function eliminar($id){
        $this->ingreso->set("id", $id);
        $this->ingreso->eliminar();
    }
    

    public function ver($ingr_id){
        $this->ingreso->set("ingr_id", $ingr_id);
        $datos = $this->ingreso->ver();
        return $datos;
    }

}
