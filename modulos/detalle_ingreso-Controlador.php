<?php
include_once ('clases/detalle_ingreso-Modelo.php');
class detalle_ingresoControlador{
    //atributos
    private $detalle_ingreso;

public function __construct() {
    $this->detalle_ingreso = new detalle_ingreso();
    }

public function index($ingr_id){
        $this->detalle_ingreso->set("ingr_id", $ingr_id);
        $resultado=$this->detalle_ingreso->listar();  
        return$resultado;
    }

public function crear($deta_ingr_cant, $ingr_id, $prod_id){
    $this->detalle_ingreso->set("deta_ingr_cant", $deta_ingr_cant);
        $this->detalle_ingreso->set("ingr_id", $ingr_id);
        $this->detalle_ingreso->set("prod_id", $prod_id);
        
    $resultado = $this->detalle_ingreso->crear();
        return $resultado;

    }
public function editar($deta_ingr_id, $deta_ingr_cant, $prod_id){
    $this->detalle_ingreso->set("deta_ingr_id", $deta_ingr_id);
        $this->detalle_ingreso->set("deta_ingr_cant", $deta_ingr_cant);
        $this->detalle_ingreso->set("prod_id", $prod_id);
        $this->detalle_ingreso->editar();

}

    public function eliminar($id){
        $this->detalle_ingreso->set("id", $id);
        $this->detalle_ingreso->eliminar();
    }

    public function eliminarall($id){
        $this->detalle_ingreso->set("id", $id);
        $this->detalle_ingreso->eliminarall();
    }
    

    public function ver($prod_id, $luga_id){
        $this->detalle_ingreso->set("prod_id", $prod_id);
        $this->detalle_ingreso->set("luga_id", $luga_id);
        $datos = $this->detalle_ingreso->ver();
        return $datos;
    }

}
