<?php


include_once('clases/permiso.php');

class controladorPermiso{
    //atributos
    private $permiso;
    
    //metodos
    public function __construct() {
        $this->permiso = new permiso();
    }
    public function index(){
        $resultado = $this->permiso->listar();  
        return $resultado;
    }
    public function crear($lugar_id, $usuario_id, $tipo_permiso_id){
        $this->permiso->set("lugar_id", $lugar_id);
        $this->permiso->set("usuario_id", $usuario_id);
        $this->permiso->set("tipo_permiso_id", $tipo_permiso_id);
        
        $resultado = $this->permiso->crear();
        return $resultado;
    }
    public function eliminar($id){
        $this->permiso->set("id", $id);
        $this->permiso->eliminar();
    }
    public function ver($id){
        $this->permiso->set("id", $id);
        $datos = $this->permiso->ver();
        return $datos;
    }
    public function editar($id, $lugar_id, $usuario_id, $tipo_permiso_id){
        $this->permiso->set("id", $id);
        $this->permiso->set("lugar_id", $lugar_id);
        $this->permiso->set("usuario_id", $usuario_id);
        $this->permiso->set("tipo_permiso_id", $tipo_permiso_id);
        $this->permiso->editar();
    }

    
    public function LugarUsuario($usuario_id){
        $this->permiso->set("usuario_id", $usuario_id);
        $resultado = $this->permiso->listarLugaresUsuarios();
        return $resultado;
    } 
}

