<?php
include_once ('clases/stock_registro-Modelo.php');
class stock_registroControlador{
    //atributos
    private $stock_registro;

public function __construct() {
    $this->stock_registro = new stock_registro();
    }

public function index(){
        $resultado=$this->stock_registro->listar();  
        return$resultado;
    }
    
public function listarDetalle($fecha){
        $this->stock_registro->set("fecha", $fecha);
        $resultado=$this->stock_registro->listarDetalle();  
        return$resultado;
    }

public function crear($stock_registro_fecha, $prod_id, $n943, $estacion, $plaza, $hj, $deposito, $total){
    $this->stock_registro->set("stock_registro_fecha", $stock_registro_fecha);
        $this->stock_registro->set("prod_id", $prod_id);
        $this->stock_registro->set("n943", $n943);
        $this->stock_registro->set("estacion", $estacion);
        $this->stock_registro->set("plaza", $plaza);
        $this->stock_registro->set("hj", $hj);
        $this->stock_registro->set("deposito", $deposito);
        $this->stock_registro->set("total", $total);
        
    $resultado = $this->stock_registro->crear();
        return $resultado;

    }
public function editar($stock_registro_id, $stock_registro_fecha, $prod_id, $n943, $estacion, $plaza, $hj, $deposito, $total){
    $this->stock_registro->set("stock_registro_id", $stock_registro_id);
        $this->stock_registro->set("stock_registro_fecha", $stock_registro_fecha);
        $this->stock_registro->set("prod_id", $prod_id);
        $this->stock_registro->set("n943", $n943);
        $this->stock_registro->set("estacion", $estacion);
        $this->stock_registro->set("plaza", $plaza);
        $this->stock_registro->set("hj", $hj);
        $this->stock_registro->set("deposito", $deposito);
        $this->stock_registro->set("total", $total);
        $this->stock_registro->editar();

}

    public function eliminar($id){
        $this->stock_registro->set("id", $id);
        $this->stock_registro->eliminar();
    }
    

    public function ver(){
        $datos = $this->stock_registro->ver();
        return $datos;
    }

}
