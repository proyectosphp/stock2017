<?php
include_once ('clases/usuario-Modelo.php');
class usuarioControlador{
    //atributos
    private $usuario;

public function __construct() {
    $this->usuario = new usuario();
    }

public function index(){
        $resultado=$this->usuario->listar();  
        return$resultado;
    }

public function crear($usua_nomb, $usua_pass){
    $this->usuario->set("usua_nomb", $usua_nomb);
        $this->usuario->set("usua_pass", $usua_pass);
        
    $resultado = $this->usuario->crear();
        return $resultado;

    }
public function editar($usua_id, $usua_nomb, $usua_pass){
    $this->usuario->set("usua_id", $usua_id);
        $this->usuario->set("usua_nomb", $usua_nomb);
        $this->usuario->set("usua_pass", $usua_pass);
        $this->usuario->editar();

}

    public function eliminar($id){
        $this->usuario->set("id", $id);
        $this->usuario->eliminar();
    }
    

    public function ver(){
        $datos = $this->usuario->ver();
        return $datos;
    }

    public function login($nombre, $pass){
        
        $this->usuario->set("nombre", $nombre);
        $this->usuario->set("pass", $pass);
        
        $datos=$this->usuario->login();
        if (isset($datos)) {
            
            $_SESSION['logeo'] = array('id'=> $datos['usua_id'], 'nombre' => $datos['usua_nomb']);

            return $datos;           
        }  else {
            return null;
        }  
    }
    
    public function logout (){
        session_unset();
        session_destroy();
        header("Location: index.php");
        
    }

}
