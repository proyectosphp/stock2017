<?php
include_once ('clases/producto-Modelo.php');
class productoControlador{
    //atributos
    private $producto;

public function __construct() {
    $this->producto = new producto();
    }

public function index(){
        $resultado=$this->producto->listar();  
        return$resultado;
    }
	
public function indexDepo(){
        $resultado=$this->producto->listarDepo();  
        return$resultado;
    }


public function crear($prod_nomb, $prod_cara, $prod_cate_id){
    $this->producto->set("prod_nomb", $prod_nomb);
        $this->producto->set("prod_cara", $prod_cara);
        $this->producto->set("prod_cate_id", $prod_cate_id);
        
    $resultado = $this->producto->crear();
        return $resultado;

    }
public function editar($prod_id, $prod_nomb, $prod_cara, $prod_cate_id){
    $this->producto->set("prod_id", $prod_id);
        $this->producto->set("prod_nomb", $prod_nomb);
        $this->producto->set("prod_cara", $prod_cara);
        $this->producto->set("prod_cate_id", $prod_cate_id);
        $this->producto->editar();

}

    public function eliminar($id){
        $this->producto->set("id", $id);
        $this->producto->eliminar();
    }
    

    public function ver(){
        $datos = $this->producto->ver();
        return $datos;
    }

}
