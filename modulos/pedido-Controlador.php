<?php
include_once ('clases/pedido-Modelo.php');
class pedidoControlador{
    //atributos
    private $pedido;

public function __construct() {
    $this->pedido = new pedido();
    }

public function index($fechaini, $fechafin, $luga_id){
        $this->pedido->set("luga_id", $luga_id);
        $this->pedido->set("fechaini", $fechaini);
        $this->pedido->set("fechafin", $fechafin);
        $resultado=$this->pedido->listar();  
        return$resultado;
    }

public function listaconcombo($fechaini, $fechafin, $luga_id, $esta_pedi_id){
        $this->pedido->set("esta_pedi_id", $esta_pedi_id);
        $this->pedido->set("luga_id", $luga_id);
        $this->pedido->set("fechaini", $fechaini);
        $this->pedido->set("fechafin", $fechafin);
        $resultado=$this->pedido->listarconcombo();  
        return$resultado;
    }

public function indexdepo($fechaini, $fechafin){
        $this->pedido->set("fechaini", $fechaini);
        $this->pedido->set("fechafin", $fechafin);
        $resultado=$this->pedido->listardepo();  
        return$resultado;
    }

public function listaconcombodepo($fechaini, $fechafin, $esta_pedi_id){
        $this->pedido->set("esta_pedi_id", $esta_pedi_id);
        $this->pedido->set("fechaini", $fechaini);
        $this->pedido->set("fechafin", $fechafin);
        $resultado=$this->pedido->listarconcombodepo();  
        return$resultado;
    }

public function crear($pedi_nomb, $pedi_fech, $usua_id, $luga_id){
    $this->pedido->set("pedi_nomb", $pedi_nomb);
        $this->pedido->set("pedi_fech", $pedi_fech);
        $this->pedido->set("usua_id", $usua_id);
        $this->pedido->set("luga_id", $luga_id);
        
    $resultado = $this->pedido->crear();
        return $resultado;

    }
public function editar($pedi_id, $pedi_nomb){
    $this->pedido->set("pedi_id", $pedi_id);
        $this->pedido->set("pedi_nomb", $pedi_nomb);
        $this->pedido->editar();

}

    public function eliminar($id){
        $this->pedido->set("id", $id);
        $this->pedido->eliminar();
    }

    public function eliminarprod($id){
        $this->pedido->set("id", $id);
        $this->pedido->eliminarprod();
    }
    

    public function ver($pedi_id){
        $this->pedido->set("pedi_id", $pedi_id);
        $datos = $this->pedido->ver();
        return $datos;
    }

}
