<?php

class enrutador{
    public function cargarVista($vista){
        switch ($vista):
            //registro de stock
            case "stock_registro-Controlador_backup":
                include_once('modulos/'.$vista.'.php');
                break;
            
            // registro de stock
            case "crearstock_registro":
                include_once('vistas/stock_registro/'.$vista.'.php');
                break;
            
            case "editarstock_registro":
                include_once('vistas/stock_registro/'.$vista.'.php');
                break;
            
            case "eliminarstock_registro":
                include_once('vistas/stock_registro/'.$vista.'.php');
                break;
            
            case "liststock_registro":
                include_once('vistas/stock_registro/'.$vista.'.php');
                break;
            
            case "selectstock_registro":
                include_once('vistas/stock_registro/'.$vista.'.php');
                break;
            case "selectstock_registro_detalle":
                include_once('vistas/stock_registro/'.$vista.'.php');
                break;
            
            //Usuario
            case "crearusuario":
                include_once('vistas/usuario/'.$vista.'.php');
                break;
            
            case "selectusuario":
                include_once('vistas/usuario/'.$vista.'.php');
                break;
            
            case "editarusuario":
                include_once('vistas/usuario/'.$vista.'.php');
                break;
            
            case "eliminarusuario":
                include_once('vistas/usuario/'.$vista.'.php');
                break;
            
            case "listusuario":
                include_once('vistas/usuario/'.$vista.'.php');
                break;

            //Login
            
            case "login":
                include_once('vistas/login/'.$vista.'.php');
                break;
            case "logout":
                include_once('vistas/login/'.$vista.'.php');
                break;

            //Lugar
            
            case "crearLugar":
                include_once('vistas/lugar/'.$vista.'.php');
                break;
            
            case "verLugar":
                include_once('vistas/lugar/'.$vista.'.php');
                break;
            
            case "editarLugar":
                include_once('vistas/lugar/'.$vista.'.php');
                break;
            
            case "eliminarLugar":
                include_once('vistas/lugar/'.$vista.'.php');
                break;
            
            case "listLugar":
                include_once('vistas/lugar/'.$vista.'.php');
                break;

            //Categoria
            
            case "crearproducto_categoria":
                include_once('vistas/producto_categoria/'.$vista.'.php');
                break;
            
            case "selectproducto_categoria":
                include_once('vistas/producto_categoria/'.$vista.'.php');
                break;
            
            case "editarproducto_categoria":
                include_once('vistas/producto_categoria/'.$vista.'.php');
                break;
            
            case "eliminarproducto_categoria":
                include_once('vistas/producto_categoria/'.$vista.'.php');
                break;
            
            case "listproducto_categoria":
                include_once('vistas/producto_categoria/'.$vista.'.php');
                break;

            //Producto
            
            case "crearproducto":
                include_once('vistas/producto/'.$vista.'.php');
                break;
            
            case "selectproducto":
                include_once('vistas/producto/'.$vista.'.php');
                break;

            case "selectproductoDepo":
                include_once('vistas/producto/'.$vista.'.php');
                break;
            
            case "editarproducto":
                include_once('vistas/producto/'.$vista.'.php');
                break;
            
            case "eliminarproducto":
                include_once('vistas/producto/'.$vista.'.php');
                break;
            
            case "listproducto":
                include_once('vistas/producto/'.$vista.'.php');
                break;

            case "selectcmbproducto_categoria":
                include_once('vistas/producto/'.$vista.'.php');
                break;
            

            case "listPermiso":
                include_once('vistas/permiso/'.$vista.'.php');
                break;
            
            case "variableSession":
                include_once('vistas/permiso/'.$vista.'.php');
                break;
           
            case "borrarVariablePermisos":
                include_once('vistas/permiso/'.$vista.'.php');
                break;
            
            case "listLugaresUsuario":
                include_once('vistas/permiso/'.$vista.'.php');
                break;
            
            //informacion
            
            case "informacion":
                include_once('vistas/informacion/'.$vista.'.php');
                break;
            
            case "detalleStock":
                include_once('vistas/informacion/'.$vista.'.php');
                break;
            
            case "totalStock":
                include_once('vistas/informacion/'.$vista.'.php');
                break;
            
            case "todoStock":
                include_once('vistas/informacion/'.$vista.'.php');
                break;
            
            //pedidos
 
            case "crearpedido":
                include_once('vistas/pedido/'.$vista.'.php');
                break;
            
            case "selectpedido":
                include_once('vistas/pedido/'.$vista.'.php');
                break;
            
            case "editarpedido":
                include_once('vistas/pedido/'.$vista.'.php');
                break;
            
            case "eliminarpedido":
                include_once('vistas/pedido/'.$vista.'.php');
                break;
            
            case "listpedido":
                include_once('vistas/pedido/'.$vista.'.php');
                break;

            //Detalle pedido
 
            case "creardetalle_pedido":
                include_once('vistas/detalle_pedido/'.$vista.'.php');
                break;
            
            case "selectdetalle_pedido":
                include_once('vistas/detalle_pedido/'.$vista.'.php');
                break;
            
            case "editardetalle_pedido":
                include_once('vistas/detalle_pedido/'.$vista.'.php');
                break;
            
            case "eliminardetalle_pedido":
                include_once('vistas/detalle_pedido/'.$vista.'.php');
                break;
            
            case "listdetalle_pedido":
                include_once('vistas/detalle_pedido/'.$vista.'.php');
                break;

            case "procesos":
                include_once('vistas/detalle_pedido/'.$vista.'.php');
                break;

            //Ingreso
            
            case "crearingreso":
                include_once('vistas/ingreso/'.$vista.'.php');
                break;
            
            case "selectingreso":
                include_once('vistas/ingreso/'.$vista.'.php');
                break;
            
            case "editaringreso":
                include_once('vistas/ingreso/'.$vista.'.php');
                break;
            
            case "eliminaringreso":
                include_once('vistas/ingreso/'.$vista.'.php');
                break;
            
            case "listingreso":
                include_once('vistas/ingreso/'.$vista.'.php');
                break;

            //Detalle Ingreso
            case "creardetalle_ingreso":
                include_once('vistas/detalle_ingreso/'.$vista.'.php');
                break;
            
            case "eliminardetalle_ingreso":
                include_once('vistas/detalle_ingreso/'.$vista.'.php');
                break;
            
            case "editardetalle_ingreso":
                include_once('vistas/detalle_ingreso/'.$vista.'.php');
                break;
            
            case "selectdetalle_ingreso":
                include_once('vistas/detalle_ingreso/'.$vista.'.php');
                break;

            case "listdetalle_ingreso":
                include_once('vistas/detalle_ingreso/'.$vista.'.php');
                break;

            case "guardarstock":
                include_once('vistas/detalle_ingreso/'.$vista.'.php');
                break;
            //stock
            case "crearstock":
                include_once('vistas/stock/'.$vista.'.php');
                break;
            case "editarstock":
                include_once('vistas/stock/'.$vista.'.php');
                break;
            case "eliminarstock":
                include_once('vistas/stock/'.$vista.'.php');
                break;
            case "liststock":
                include_once('vistas/stock/'.$vista.'.php');
                break;
            case "selectstock":
                include_once('vistas/stock/'.$vista.'.php');
                break;
            
            default:
                include_once('vistas/error.php');
                
            
        endswitch;
    }
    
    
}