<?php


include_once('clases/PedidoStockIngreso.php');

class controladorPedidoStockIngreso{
    //atributos
    private $PedidoStockIngreso;
    
    //metodos
    public function __construct() {
        $this->PedidoStockIngreso = new PedidoStockIngreso();
    }
    public function index(){
        $resultado = $this->pedido->listar();  
        return $resultado;
    }
    public function crear($nombre, $fech, $usuaid, $lugaid, $estaid){
        $this->pedido->set("nombre", $nombre);
        $this->pedido->set("fech", $fech);
        $this->pedido->set("usuaid", $usuaid);
        $this->pedido->set("lugaid", $lugaid);
        $this->pedido->set("estaid", $estaid);
        
        $resultado = $this->pedido->crear();
        return $resultado;
    }
    public function eliminar($id){
        $this->pedido->set("id", $id);
        $this->pedido->eliminar();
    }
    
    private function detallePedido($pedi_id){
        $this->PedidoStockIngreso->set("pedi_id", $pedi_id);
        
        $resultado = $this->PedidoStockIngreso->detallePedido();
        return $resultado;
    }
    
    private function sumarPedidoAStock($deta_pedi_cant_acep, $luga_id, $prod_id ){
        $this->PedidoStockIngreso->set("deta_pedi_cant_acep", $deta_pedi_cant_acep);
        $this->PedidoStockIngreso->set("luga_id", $luga_id);
        $this->PedidoStockIngreso->set("prod_id", $prod_id);
        $this->PedidoStockIngreso->sumarPedidoAStock();
    }
    
    private function SumarPedidoEntreAStock($deta_pedi_cant_acep, $luga_id, $prod_id ){
        $this->PedidoStockIngreso->set("deta_pedi_cant_acep", $deta_pedi_cant_acep);
        $this->PedidoStockIngreso->set("luga_id", $luga_id);
        $this->PedidoStockIngreso->set("prod_id", $prod_id);
        $this->PedidoStockIngreso->SumarPedidoEntreAStock();
    }
    
    private function RestarPedidoEntreAStock($deta_pedi_cant_acep, $luga_id, $prod_id ){
        $this->PedidoStockIngreso->set("deta_pedi_cant_acep", $deta_pedi_cant_acep);
        $this->PedidoStockIngreso->set("luga_id", $luga_id);
        $this->PedidoStockIngreso->set("prod_id", $prod_id);
        $this->PedidoStockIngreso->RestarPedidoEntreAStock();
    }
        
    public function sumPedido($pedi_id){
        $this->PedidoStockIngreso->set("pedi_id", $pedi_id);
        $this->PedidoStockIngreso->sumPedido();
    }
    
    public function EntrePedido($pedi_id){
        $this->PedidoStockIngreso->set("pedi_id", $pedi_id);
        $this->PedidoStockIngreso->EntrePedido();
    }
    
    public function EnProcRealPedido($pedi_id){
        $this->PedidoStockIngreso->set("pedi_id", $pedi_id);
        $this->PedidoStockIngreso->EnProcRealPedido();
    }
          
    public function ProcesosumarPedidoAStock($pedi_id ){
        
       $detallepedido= new controladorPedidoStockIngreso();
       $detallepedido1= new controladorPedidoStockIngreso();
       
       $detallesuma = $detallepedido ->detallePedido($pedi_id);
       
       while($row = mysql_fetch_assoc($detallesuma)){
           
         $detallepedido1 ->sumarPedidoAStock($row['deta_pedi_cant_acep'], $row['luga_id'], $row['prod_id']);
         
        }
       $detallepedido1->sumPedido($pedi_id);    
       
    }
    
    
    public function ProcesosumarPedidoEntreAStock($pedi_id ){
        
       $detallepedido= new controladorPedidoStockIngreso();
       $detallepedido1= new controladorPedidoStockIngreso();
       
       $detallesuma = $detallepedido ->detallePedido($pedi_id);
       
       while($row = mysql_fetch_assoc($detallesuma)){
           
         $detallepedido1 ->SumarPedidoEntreAStock($row['deta_pedi_cant_acep'], $row['luga_id'], $row['prod_id']);
         
        }
       $detallepedido1->EnProcRealPedido($pedi_id);    
       
    }
    
    public function ProcesorestarPedidoEntreAStock($pedi_id ){
        
       $detallepedido= new controladorPedidoStockIngreso();
       $detallepedido1= new controladorPedidoStockIngreso();
       
       $detallesuma = $detallepedido ->detallePedido($pedi_id);
       
       while($row = mysql_fetch_assoc($detallesuma)){
           
         $detallepedido1 ->RestarPedidoEntreAStock($row['deta_pedi_cant_acep'], $row['luga_id'], $row['prod_id']);
         
        }
       $detallepedido1->EntrePedido($pedi_id);    
       
    }
    
    public function controlarProductoRepetido($pedi_id, $prod_id ){
        $this->PedidoStockIngreso->set("pedi_id", $pedi_id);
        $this->PedidoStockIngreso->set("prod_id", $prod_id);
        $resultado=$this->PedidoStockIngreso->controlarProductoRepetido();
        return $resultado;
        
    }
    
}

?>