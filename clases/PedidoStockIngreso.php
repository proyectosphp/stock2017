<?php
//clase de conexion incluida
include_once 'conexion.php';


class PedidoStockIngreso{
    //atributos
    
    private $stoc_cant_disp;
    private $stoc_cant_fisi;
    private $prod_id;
    private $luga_id;
    private $deta_pedi_cant_acep;
    private $pedi_id;
 
    
    private $con;


    //metodos
    public function __construct() {
        $this->con = new conexion();
    }
    public function set($atributo, $contenido){
        $this->$atributo = $contenido;
    }
    public function get($atributo){
        return $this->$atributo;
    }
    
    public function eliminar(){
        $sql= "delete from stock where stoc_id='{$this->stoc_id}'";
        $this->con->consultaSimple($sql);
    }
    public function ver(){
        $sql= "select * from stock where stoc_id='{$this->stoc_id}'";
        $resultado = $this->con->consultaRetorno($sql);
        $row = mysql_fetch_assoc($resultado);
        return $row;    
    }
    
    public function detallePedido(){
        $sql= "
        SELECT pedido.luga_id, detalle_pedido.deta_pedi_cant_acep, detalle_pedido.prod_id FROM `detalle_pedido` 
        INNER join pedido on pedido.pedi_id = detalle_pedido.pedi_id
        WHERE detalle_pedido.pedi_id='{$this->pedi_id}'";
        $resultado = $this->con->consultaRetorno($sql);
        
        return $resultado;    
    }
    
    
    public function sumarPedidoAStock(){
        $sql= "
        UPDATE `stock`
        SET 
        stock.stoc_cant_disp = stoc_cant_disp + '{$this->deta_pedi_cant_acep}',
        stock.stoc_cant_fisi = stoc_cant_fisi + '{$this->deta_pedi_cant_acep}',
        stock.stoc_fech_modi = now()
        WHERE 
        stock.luga_id='{$this->luga_id}' And stock.prod_id='{$this->prod_id}'";
        $this->con->consultaRetorno($sql);
        
    }
    
    
    public function RestarPedidoEntreAStock(){
        $sql= "
        UPDATE `stock`
        SET
        stock.stoc_cant_fisi = stoc_cant_fisi - '{$this->deta_pedi_cant_acep}',
        stock.stoc_fech_modi = now()
        WHERE 
        stock.luga_id='{$this->luga_id}' And stock.prod_id='{$this->prod_id}'";
        $this->con->consultaRetorno($sql);
        
    }
    
    public function SumarPedidoEntreAStock(){
        $sql= "
        UPDATE `stock`
        SET
        stock.stoc_cant_fisi = stoc_cant_fisi + '{$this->deta_pedi_cant_acep}',
        stock.stoc_fech_modi = now()
        WHERE 
        stock.luga_id='{$this->luga_id}' And stock.prod_id='{$this->prod_id}'";
        $this->con->consultaRetorno($sql);
        
    }
    
    
    public function sumPedido(){
        $sql= "
        UPDATE `pedido`
        SET 
        pedido.pedi_sum_stock = 1
        WHERE 
        pedido.pedi_id='{$this->pedi_id}'";
        $this->con->consultaRetorno($sql);
    }
    
    public function EnProcRealPedido(){
        $sql= "
        UPDATE `pedido`
        SET 
        pedido.pedi_sum_stock = 0,
        pedido.pedi_rest_fisi = 0
        WHERE 
        pedido.pedi_id='{$this->pedi_id}'";
        $this->con->consultaRetorno($sql);
    }
    
    
    public function EntrePedido(){
        $sql= "
        UPDATE `pedido`
        SET
        pedido.pedi_rest_fisi = 1
        WHERE 
        pedido.pedi_id='{$this->pedi_id}'";
        $this->con->consultaRetorno($sql);
    }
    
    public function controlarProductoRepetido(){
        $sql= "select * from detalle_pedido INNER JOIN producto on producto.prod_id=detalle_pedido.prod_id INNER JOIN pedido on pedido.pedi_id=detalle_pedido.pedi_id where detalle_pedido.pedi_id='{$this->pedi_id}' AND detalle_pedido.prod_id='{$this->prod_id}'";
        $resultado = $this->con->consultaRetorno($sql);
        $num = mysql_num_rows($resultado);
        if($num!=0){
            return false;
        }else{
            return true;
        }
        
    }
    
    
}



