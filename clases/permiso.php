<?php

include_once 'conexion.php';

class permiso{
    //atributos
    private $id;
    private $lugar_id;
    private $usuario_id;
    private $tipo_permiso_id;


    //metodos
    public function __construct() {
        $this->con = new conexion();
    }
    public function set($atributo, $contenido){
        $this->$atributo = $contenido;
    }
    public function get($atributo){
        return $this->$atributo;
    }
    public function crear(){
           $sql="INSERT INTO detalle_permiso (luga_id, usua_id, tipo_perm_id) VALUES ('{$this->lugar_id}', '{$this->usuario_id}', '{$this->tipo_permiso_id}')"; 
           $this->con->consultaSimple($sql);
             
    }
    public function eliminar(){
        $sql= "delete from detalle_permiso where usua_id = '{$this->id}'";
        $this->con->consultaSimple($sql);
    }
    public function ver(){
        $sql= "select * from detalle_permiso where usua_id='{$this->usuario_id}'";
        $resultado = $this->con->consultaRetorno($sql);
        $row = mysql_fetch_assoc($resultado);
        return $row;
        
    }
    
    public function editar(){
        $sql= "update detalle_permiso set luga_id='{$this->lugar_id}', usua_id='{$this->usuario_id}', tipo_perm_id='{$this->tipo_permiso_id}' where deta_perm_id = '{$this->id}'";
        $this->con->consultaSimple($sql);
    }
    
    public function listar(){
        $sql= " 
            SELECT  
            usuario.usua_nomb, usuario.usua_id, lugar.luga_id, lugar.luga_nomb, tipo_permiso.tipo_perm_nomb
            FROM `detalle_permiso` 
            INNER JOIN usuario on usuario.usua_id=detalle_permiso.usua_id
            INNER JOIN lugar ON lugar.luga_id=detalle_permiso.luga_id
            INNER JOIN tipo_permiso ON tipo_permiso.tipo_perm_id=detalle_permiso.tipo_perm_id
            ";
        $resultado = $this->con->consultaRetorno($sql);
        return $resultado;
    }
    
    public function listarLugaresUsuarios(){
        $sql= "SELECT usuario.usua_nomb, usuario.usua_id, lugar.luga_id, lugar.luga_depo, lugar.luga_nomb, tipo_permiso.tipo_perm_nomb FROM `detalle_permiso` INNER JOIN usuario on usuario.usua_id=detalle_permiso.usua_id INNER JOIN lugar ON lugar.luga_id=detalle_permiso.luga_id INNER JOIN tipo_permiso ON tipo_permiso.tipo_perm_id=detalle_permiso.tipo_perm_id Where usuario.usua_id='{$this->usuario_id}'";
        $resultado = $this->con->consultaRetorno($sql);
        return $resultado;
    }
    
    
}
