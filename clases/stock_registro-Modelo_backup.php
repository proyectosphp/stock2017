<?php

include_once 'conexion.php';

class stock_registroBackup{
    //atributos
    private $con;
    private $fecha;
    private $prod_id;
    private $nueve;
    private $estacion;
    private $plaza;
    private $howard;
    private $deposito;
    private $total;
   
    public function __construct() {
        $this->con = new conexion();
    }
    //metodos
    
    public function set($atributo, $contenido){
        $this->$atributo = $contenido;
    }
     
    
    
    public function todoStock(){
        $sql= " 
            SELECT producto.prod_id, 
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=5) as nueve,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=6) as estacion,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=7) as plaza,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=8) as howard,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=9) as deposito,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id) as total
            from producto
            ORDER by producto.prod_nomb
            ";
        $resultado = $this->con->consultaRetorno($sql);
        return $resultado;
    }
    
        public function crear(){
           $sql="INSERT INTO `stock_registro`(`stock_registro_fecha`, `prod_id`, `n943`, `estacion`, `plaza`, `hj`, `deposito`, `total`) VALUES ( '{$this->fecha}','{$this->prod_id}', '{$this->nueve}', '{$this->estacion}', '{$this->plaza}', '{$this->howard}', '{$this->deposito}', '{$this->total}')"; 
           $this->con->consultaSimple($sql);
           return true;
        
         
    }
  
}


