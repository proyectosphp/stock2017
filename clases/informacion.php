<?php

include_once 'conexion.php';

class informacion{
    //atributos
    private $idProoducto;
    private $con;
   
    public function __construct() {
        $this->con = new conexion();
    }
    //metodos
    
    public function set($atributo, $contenido){
        $this->$atributo = $contenido;
    }
     
    public function detalleStock(){
        $sql= " 
            SELECT lugar.luga_nomb, producto.prod_nomb, stock.stoc_cant_fisi FROM `stock` 
            inner JOIN producto on producto.prod_id = stock.prod_id
            inner join lugar on lugar.luga_id=stock.luga_id
            ORDER BY stock.prod_id
            ";
        $resultado = $this->con->consultaRetorno($sql);
        return $resultado;
    }
    
    public function productosStock(){
        $sql= " 
            SELECT * from producto
            ";
        $resultadoProductos = $this->con->consultaRetorno($sql);
        return $resultadoProductos;
    }
    
    public function sumaProducto(){
        $sql= " 
            SELECT SUM(stock.stoc_cant_fisi), producto.prod_nomb FROM `stock`
            INNER JOIN producto on producto.prod_id=stock.prod_id
            WHERE stock.prod_id='{$this->idProoducto}'
            ";
        $resultadoSuma = $this->con->consultaRetorno($sql);
        return $resultadoSuma;
    }
    
    public function todoStock(){
        $sql= " 
            SELECT producto.prod_nomb, 
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=5) as nueve,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=6) as estacion,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=7) as plaza,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=8) as howard,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id and stock.luga_id=9) as deposito,
            (SELECT SUM(stock.stoc_cant_fisi) FROM stock WHERE stock.prod_id=producto.prod_id) as total
            from producto
            ORDER by producto.prod_nomb
            ";
        $resultado = $this->con->consultaRetorno($sql);
        return $resultado;
    }
    
    
    

    
}


