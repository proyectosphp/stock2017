<?php
//clase de conexion incluida
include_once 'conexion.php';

class detalle_pedido{
private $deta_pedi_id;
private $deta_pedi_cant_pedi;
private $deta_pedi_cant_acep;
private $pedi_id;
private $prod_id;
private $luga_id;
//metodos
    public function __construct() {
        $this->con = new conexion();
    }
    public function set($atributo, $contenido){
        $this->$atributo = $contenido;
    }
    public function get($atributo){
        return $this->$atributo;
    }
    
 
public function eliminar(){
        $sql= 
        "delete from detalle_pedido where deta_pedi_id ='$this->id'";
        $this->con->consultaSimple($sql);
    }
 
public function ver(){
        $sql="select * from stock where prod_id='$this->prod_id' and luga_id='$this->luga_id'";
        $resultado = $this->con->consultaRetorno($sql);
        return $resultado;
    }

public function crear(){
        $sql="INSERT INTO detalle_pedido (deta_pedi_cant_pedi, deta_pedi_cant_acep, pedi_id, prod_id) VALUES ('{$this->deta_pedi_cant_pedi}','{$this->deta_pedi_cant_acep}','{$this->pedi_id}','{$this->prod_id}')";
    $this->con->consultaSimple($sql);
    }
public function editar(){
        $sql="update detalle_pedido set deta_pedi_cant_pedi='{$this->deta_pedi_cant_pedi}', deta_pedi_cant_acep='{$this->deta_pedi_cant_acep}', prod_id='{$this->prod_id}' where deta_pedi_id ='$this->deta_pedi_id'";
        $this->con->consultaSimple($sql);
    }
public function editardepo(){
        $sql="update detalle_pedido set deta_pedi_cant_acep='{$this->deta_pedi_cant_acep}' where deta_pedi_id ='$this->deta_pedi_id'";
        $this->con->consultaSimple($sql);
    }
public function listar(){
        $sql="select *, (SELECT stoc_cant_fisi FROM stock WHERE stock.prod_id = detalle_pedido.prod_id and stock.luga_id= '$this->luga_id') as deposito_stock from detalle_pedido inner join producto on producto.prod_id=detalle_pedido.prod_id where detalle_pedido.pedi_id='{$this->pedi_id}'";
    $resultado = $this->con->consultaRetorno($sql);
        return $resultado;
    }

public function editarrealizado(){
        $sql="update pedido set esta_pedi_id=1 where pedi_id ='$this->pedi_id'";
        $this->con->consultaSimple($sql);
    }

public function editaraceptado(){
        $sql="update pedido set esta_pedi_id=3 where pedi_id ='$this->pedi_id'";
        $this->con->consultaSimple($sql);
    }

public function editarrechazado(){
        $sql="update pedido set esta_pedi_id=2 where pedi_id ='$this->pedi_id'";
        $this->con->consultaSimple($sql);
    }

public function editarrecibido(){
        $sql="update pedido set esta_pedi_id=4 where pedi_id ='$this->pedi_id'";
        $this->con->consultaSimple($sql);
    }

public function listarstock(){

        $sql= "select * from stock inner join producto on producto.prod_id=stock.prod_id where luga_id= '{$this->luga_id}' and stock.prod_id= '{$this->prod_id}'";
        $resultado = $this->con->consultaRetorno($sql);
        return $resultado;
    }

}