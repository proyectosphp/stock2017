<?php 
if (isset($_SESSION['logeo'])){
        $form=$_SESSION['logeo'];
    }
    if(!isset($form)){
        echo "<SCRIPT LANGUAGE=JavaScript> 
                window.location.href='index.php';
            </SCRIPT>";
    }
if (isset($_SESSION['permisos'])){
        $permisos=$_SESSION['permisos'];
    }
$controlador = new ingresoControlador();
$row=$controlador->ver($_GET['idingr']);
?>  
<div id="detalle_ingreso">
<h1 align="center">Lista de Productos a Ingresar</h1>

<nav>
            <ul>
                <?php
                if($row['ingr_ingre']=="No Ingresado"){
                    echo '<a class="btn btn-info"  onclick="nuevodetalle_ingreso()">Nuevo</a>';
                    }

                 ?>
                <a class="btn btn-info"  href="index.php?cargar=listingreso">Volver</a>
                <?php
                if($row['ingr_ingre']=="No Ingresado"){
                    echo '<a class="btn btn-info"  onclick="guardarstock(dataprod)">OK</a>';
                    }

                 ?>
                
            </ul>
</nav> 
<div class="input-group" style="width: 280px; margin: 0 auto;">
    <input id="filtrar" type="text" class="form-control" placeholder="Buscar...">
    <br><br>
</div>

<div class="table-responsive col-md-10 col-md-offset-1">
<table class="table table-striped" border=3>
    <thead>
    
<th>ID</th>
            <th>Cantidad a Ingresar</th>
            <th>Producto</th>
            
    <th>Opciones</th>
    </thead>
    <tbody id="tblRegistrosdetalle_ingreso" class="buscar">
        
    </tbody>
</table>
</div>
</div>
<div id="nuevo">
</div>
<div id="editar">
</div>
<div id="producto">

</div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" id="div1">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
            <script>


    $(document).ready(function () {
        var idingr = <?php echo $_GET['idingr']; ?>;
        var estadoingr = "<?php echo $row['ingr_ingre']; ?>";
        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectdetalle_ingreso&idingr='+idingr,
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.deta_ingr_id;
                        var deta_ingr_cant ="document.getElementById('deta_ingr_cant"+id+"').innerHTML";
            var prod_id ="document.getElementById('prod_id"+id+"').innerHTML";
                var prod_nomb ="document.getElementById('detalle_ingresoprod_nomb"+id+"').innerHTML";
                    var content = "";
                        content += '<tr id="' + datos.deta_ingr_id + '">';
content += '<td id="deta_ingr_id' + datos.deta_ingr_id + '">' + datos.deta_ingr_id + '</td>';
            content += '<td id="deta_ingr_cant' + datos.deta_ingr_id + '">' + datos.deta_ingr_cant + '</td>';
            content += '<td id="detalle_ingresoprod_nomb' + datos.deta_ingr_id + '">' + datos.prod_nomb + '</td>';
                content += '<td style="display:none" id="prod_id' + datos.deta_ingr_id + '">' + datos.prod_id + '</td>';
                          content += '<td>';
                        if(estadoingr=='No Ingresado'){
                            content += '<a class="btn btn-info" onclick="editardetalle_ingreso(' + datos.deta_ingr_id + ',' + deta_ingr_cant + ', ' + prod_id + ', ' + prod_nomb + ')">editar</a>';
                            content += '<a data-id="' + datos.deta_ingr_id + '" class="btn btn-danger botEliminar" >eliminar</a>';
                        }
                        
                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosdetalle_ingreso").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });



</script>

<script>
    function editardetalle_ingreso(deta_ingr_id, deta_ingr_cant, prod_id, prod_nomb) {
            $("#editar").empty();


        var content = "EDITAR";

        content += '<form  value="' + deta_ingr_id + '" id="frmdata">';
        content += '<input id="ediid" type="hidden" name="id" value="' + deta_ingr_id + '" class="form-control">';
        content += '<div class="form-group">';
            content += '<label>Cantidad a Ingresar</label>';
            content += '<input id="edideta_ingr_cant" min="0" type="number" name="deta_ingr_cant" value="' + deta_ingr_cant + '" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<a class="btn btn-info" onclick="producto('+"'"+"editar"+"'"+')">  Seleccionar Producto</a><br>';
            content += '<input type="hidden" id="prod_id" name="prod_id" value="' + prod_id + '" class="form-control" required>';
            content += '<label>Producto</label><br>';
            content += '<input type="text" id="prod_nomb" name="prod_nomb" value="' + prod_nomb + '" class="form-control" readonly>';
            content += '</div>';
            
        content += '<a type="button" href="index.php?cargar=listdetalle_ingreso" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

        content += '<button type="submit" onclick="editarrdetalle_ingreso()" class="btn btn-success">Editar</button>';
        content += '</form>';





        $("#detalle_ingreso").hide();
        $("#editar").show();
        $("#editar").append(content);
    }


</script>
<script>
    function nuevodetalle_ingreso() {

    $("#nuevo").empty();


        var content = "nuevo";

        content += '<form  id="frmdata">';
        content += '<div class="form-group">';
            content += '<label>Cantidad a Ingresar</label>';
            content += '<input type="number" min="0" name="deta_ingr_cant" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<a class="btn btn-info" onclick="producto('+"'"+"nuevo"+"'"+')">  Seleccionar Producto</a><br>';
            content += '<input type="hidden" id="prod_id" name="prod_id" class="form-control" required>';
            content += '<label>Producto</label><br>';
            content += '<input type="text" id="prod_nomb" name="prod_nomb" class="form-control" readonly>';
            content += '</div>';
            
        content += '<a type="button" href="index.php?cargar=listdetalle_ingreso" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

        content += '<button type="submit" onclick="guardardetalle_ingreso()" class="btn btn-success">Crear</button>';
        content += '</form>';



        $("#detalle_ingreso").hide();



        $("#nuevo").append(content);
        $("#nuevo").show();
    }


</script>

<script type="text/javascript">
    $(document).ready(function () {
        (function ($) {
            $('#filtrar').keyup(function () {
                var rex = new RegExp($(this).val(), 'i');
                $('.buscar tr').hide();
                $('.buscar tr').filter(function () {
                    return rex.test($(this).text());
                }).show();
            })
        }(jQuery));
    });
</script>
<script>
function guardardetalle_ingreso(){

    $("#frmdata").on("submit",function(e){
        var idingr = <?php echo $_GET['idingr']; ?>;
        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

        console.log(dataSerializada);
    
        console.log('dataSerializadddda');
                
                
                    $.ajax({
        type: 'POST',
        url : 'index.php?cargar=creardetalle_ingreso&idingr='+idingr,
        dataType: 'json',
        data:dataSerializada

    })
    .done(function( data ){
        
        console.log('Correcto!');

        console.log( data ); // Se imprime en consola la api
                window.location.href='index.php?cargar=listdetalle_ingreso&idingr='+idingr;


    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listdetalle_ingreso&idingr='+idingr;
    })
        
        



    });


    

}

</script>
<script>
function editarrdetalle_ingreso(){

    $("#frmdata").on("submit",function(e){
        var idingr = <?php echo $_GET['idingr']; ?>;
        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=editardetalle_ingreso',
        dataType: 'json',
        data:dataSerializada

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listdetalle_ingreso&idingr='+idingr;
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    })
        
        

        



    });


    

}

</script>
<script>
    $("body").on("click", ".botEliminar",function(e){
    e.preventDefault();
    var id=$(this).data('id');
    var pregunta = confirm('¿Esta seguro de eliminar esto?');
    if(pregunta==true){
        $.ajax({
        type: 'POST',
        url : '?cargar=eliminardetalle_ingreso&id='+id,
        dataType: 'json'
        });
        window.location.href='index.php?cargar=listdetalle_ingreso';
    }else{
        return false;
    }
})
</script>
<script>
    function producto(tipo) {

        $("#producto").empty();
        if(tipo=="nuevo"){
        listaproducto_nuevo();
        }else{
          listaproducto_editar();  
        }
        $("#nuevo").hide();
        $("#editar").hide();
        $("#producto").show();
        document.getElementById("producto").style.display = "";
    }
</script>

<script>  //carga los lotes por ajax mediante .load
    function listaproducto_nuevo() {

        $("#producto").load("routingjs.php?cargar=listproducto&select=nuevo");

    }
</script>

<script>  //carga los lotes por ajax mediante .load
    function listaproducto_editar() {

        $("#producto").load("routingjs.php?cargar=listproducto&select=editar");

    }
</script>


<script>
dataprod = {datosprod: []};    
    
     n=0;

    $(document).ready(function () {
        var idingr = <?php echo $_GET['idingr']; ?>;
        var luga = <?php echo $permisos['luga_id']; ?>;
        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectdetalle_ingreso&idingr='+idingr,
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {
                        n=dataprod.datosprod.length; 
        
        
                        cant=datos.deta_ingr_cant;
                        prodid=datos.prod_id;
                        dataprod.datosprod[n]={0:n,1:cant,2:prodid,3:luga};
                        
                        
                    });
                    console.log(dataprod);
                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });



</script>

<script>
function guardarstock(dataprod){

      var idingr = <?php echo $_GET['idingr']; ?>;
       $.ajax({
        type: 'POST',
        url : 'index.php?cargar=guardarstock&idingr='+idingr,
        dataType: 'json',
        data:dataprod
         

    })
    .done(function( data ){
        
        console.log('Correcto!');

        console.log( data ); // Se imprime en consola la api
        window.location.href='index.php?cargar=listdetalle_ingreso&idingr='+idingr;
        
              

    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listdetalle_ingreso&idingr='+idingr;
    })

    

}

</script> 
