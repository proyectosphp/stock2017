<div id="stock_registro">
    <h1 align="center">Lista de stock_registro</h1>

    <nav>
        <ul>
            <a class="btn btn-info" onclick="volver()">volver</a>
        </ul>
    </nav>
    <div class="input-group" style="width: 280px; margin: 0 auto;">
        <input id="filtrar" type="text" class="form-control" placeholder="Buscar...">
        <br><br>
    </div>

    <div id="detalleLista" class="table-responsive col-md-10 col-md-offset-1">
        <table class="table table-striped" border=3>
            <thead>

            <th>ID</th>
            <th>fecha</th>
            <th>producto</th>
            <th>943</th>
            <th>estacion</th>
            <th>plaza</th>
            <th>hj</th>
            <th>deposito</th>
            <th>total</th>

            <th>Opciones</th>
            </thead>
            <tbody id="tblRegistrosstock_registro" class="buscar">

            </tbody>
        </table>
    </div>

    <div id="lista" class="table-responsive col-md-10 col-md-offset-1">
        <table class="table table-striped" border=3>
            <thead>
            <th>ID</th>
            <th>fecha</th>
            <th>registros</th>
            <th>Opciones</th>
            </thead>
            <tbody id="tblLista" class="buscar">

            </tbody>
        </table>
    </div>
</div>
<div id="nuevo">
</div>
<div id="editar">
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" id="div1">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $("#lista").show();
        $("#detalleLista").hide();
        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectstock_registro',
            dataType: 'json',

        })
                .done(function (data) {
                    console.log('Correcto!');
                    console.log(data); // Se imprime en consola la api
                    data.datos.forEach(function (datos) {

                        
                        var fecha ="'"+datos.stock_registro_fecha+"'";
                        var content = "";
                        content += '<tr>';
                        content += '<td>'+ datos.stock_registro_id + '</td>';
                        content += '<td>'+datos.stock_registro_fecha+'</td>';
                        content += '<td>registro</td>';
                        content += '<td>';
                        content += '<a class="btn btn-info" onclick="DetalleLista('+fecha+')">ver</a>';
                        content += '</td>';
                        content += '</tr>';

                        $("#tblLista").append(content);
                    });

                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })

    })
    ;



</script> 
<script>
    function volver() {
        $("#lista").show();
        $("#detalleLista").hide();
    }

</script>
<script>
    function DetalleLista(fecha) {
        $("#lista").hide();
        $("#detalleLista").show();
        $("#tblRegistrosstock_registro").empty();
        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectstock_registro_detalle&fecha='+fecha,
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.stock_registro_id;
                        var stock_registro_fecha = "document.getElementById('stock_registro_fecha" + id + "').innerHTML";
                        var prod_id = "document.getElementById('prod_id" + id + "').innerHTML";
                        var n943 = "document.getElementById('n943" + id + "').innerHTML";
                        var estacion = "document.getElementById('estacion" + id + "').innerHTML";
                        var plaza = "document.getElementById('plaza" + id + "').innerHTML";
                        var hj = "document.getElementById('hj" + id + "').innerHTML";
                        var deposito = "document.getElementById('deposito" + id + "').innerHTML";
                        var total = "document.getElementById('total" + id + "').innerHTML";
                        var content = "";
                        content += '<tr id="' + datos.stock_registro_id + '">';
                        content += '<td id="stock_registro_id' + datos.stock_registro_id + '">' + datos.stock_registro_id + '</td>';
                        content += '<td id="stock_registro_fecha' + datos.stock_registro_id + '">' + datos.stock_registro_fecha + '</td>';
                        content += '<td id="prod_id' + datos.stock_registro_id + '">' + datos.prod_nomb + '</td>';
                        content += '<td id="n943' + datos.stock_registro_id + '">' + datos.n943 + '</td>';
                        content += '<td id="estacion' + datos.stock_registro_id + '">' + datos.estacion + '</td>';
                        content += '<td id="plaza' + datos.stock_registro_id + '">' + datos.plaza + '</td>';
                        content += '<td id="hj' + datos.stock_registro_id + '">' + datos.hj + '</td>';
                        content += '<td id="deposito' + datos.stock_registro_id + '">' + datos.deposito + '</td>';
                        content += '<td id="total' + datos.stock_registro_id + '">' + datos.total + '</td>';
                        content += '<td>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="verstock_registro(' + datos.stock_registro_id + ',' + stock_registro_fecha + ', ' + prod_id + ', ' + n943 + ', ' + estacion + ', ' + plaza + ', ' + hj + ', ' + deposito + ', ' + total + ')">ver</a>';


                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosstock_registro").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    }
    ;



</script>
<script>
    function verstock_registro(stock_registro_id, stock_registro_fecha, prod_id, n943, estacion, plaza, hj, deposito, total) {

        $("#div1").empty();
        var content = "ver";

        content += '<form class="form" value="' + stock_registro_id + '" id="frmdata">';
        content += '<div class="form-group">';
        content += '<label>fecha</label>';
        content += '<input type="datetime-local" name="stock_registro_fecha" value="' + stock_registro_fecha + '" class="form-control" disabled>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>producto</label>';
        content += '<input type="number" name="prod_id" value="' + prod_id + '" class="form-control" disabled>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>943</label>';
        content += '<input type="number" name="n943" value="' + n943 + '" class="form-control" disabled>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>estacion</label>';
        content += '<input type="number" name="estacion" value="' + estacion + '" class="form-control" disabled>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>plaza</label>';
        content += '<input type="number" name="plaza" value="' + plaza + '" class="form-control" disabled>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>hj</label>';
        content += '<input type="number" name="hj" value="' + hj + '" class="form-control" disabled>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>deposito</label>';
        content += '<input type="number" name="deposito" value="' + deposito + '" class="form-control" disabled>';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>total</label>';
        content += '<input type="number" name="total" value="' + total + '" class="form-control" disabled>';
        content += '</div>';

        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';
        content += '</form>';

        $("#div1").append(content);
    }


</script>
<script>
    function editarstock_registro(stock_registro_id, stock_registro_fecha, prod_id, n943, estacion, plaza, hj, deposito, total) {
        $("#div1").empty();


        var content = "EDITAR";

        content += '<form  value="' + stock_registro_id + '" id="frmdata">';
        content += '<input id="ediid" type="hidden" name="id" value="' + stock_registro_id + '" class="form-control">';
        content += '<div class="form-group">';
        content += '<label>fecha</label>';
        content += '<input id="edistock_registro_fecha" type="datetime-local" name="stock_registro_fecha" value="' + stock_registro_fecha + '" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>producto</label>';
        content += '<input id="ediprod_id" type="number" name="prod_id" value="' + prod_id + '" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>943</label>';
        content += '<input id="edin943" type="number" name="n943" value="' + n943 + '" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>estacion</label>';
        content += '<input id="ediestacion" type="number" name="estacion" value="' + estacion + '" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>plaza</label>';
        content += '<input id="ediplaza" type="number" name="plaza" value="' + plaza + '" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>hj</label>';
        content += '<input id="edihj" type="number" name="hj" value="' + hj + '" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>deposito</label>';
        content += '<input id="edideposito" type="number" name="deposito" value="' + deposito + '" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>total</label>';
        content += '<input id="editotal" type="number" name="total" value="' + total + '" class="form-control">';
        content += '</div>';

        content += '<a type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

        content += '<button type="submit" onclick="editarrstock_registro()" class="btn btn-success">Editar</button>';
        content += '</form>';






        $("#div1").append(content);
    }


</script>
<script>
    function nuevostock_registro() {

        $("#div1").empty();


        var content = "nuevo";

        content += '<form  id="frmdata">';
        content += '<div class="form-group">';
        content += '<label>fecha</label>';
        content += '<input type="datetime-local" name="stock_registro_fecha" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>producto</label>';
        content += '<input type="number" name="prod_id" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>943</label>';
        content += '<input type="number" name="n943" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>estacion</label>';
        content += '<input type="number" name="estacion" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>plaza</label>';
        content += '<input type="number" name="plaza" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>hj</label>';
        content += '<input type="number" name="hj" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>deposito</label>';
        content += '<input type="number" name="deposito" class="form-control">';
        content += '</div>';
        content += '<div class="form-group">';
        content += '<label>total</label>';
        content += '<input type="number" name="total" class="form-control">';
        content += '</div>';

        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';

        content += '<button type="submit" onclick="guardarstock_registro()" class="btn btn-success">Crear</button>';
        content += '</form>';







        $("#div1").append(content);
    }


</script>

<script type="text/javascript">
    $(document).ready(function () {
        (function ($) {
            $('#filtrar').keyup(function () {
                var rex = new RegExp($(this).val(), 'i');
                $('.buscar tr').hide();
                $('.buscar tr').filter(function () {
                    return rex.test($(this).text());
                }).show();
            })
        }(jQuery));
    });
</script>
<script>
    function guardarstock_registro() {

        $("#frmdata").on("submit", function (e) {

            e.preventDefault();

            var formulario = $(this);
            var dataSerializada = formulario.serialize();

            console.log(dataSerializada);

            console.log('dataSerializadddda');


            $.ajax({
                type: 'POST',
                url: 'index.php?cargar=crearstock_registro',
                dataType: 'json',
                data: dataSerializada

            })
                    .done(function (data) {

                        console.log('Correcto!');

                        console.log(data); // Se imprime en consola la api
                        window.location.href = 'index.php?cargar=liststock_registro';


                    })
                    .fail(function () {
                        console.log('Fallo!');
                        window.location.href = 'index.php?cargar=liststock_registro';
                    })





        });




    }

</script>
<script>
    function editarrstock_registro() {

        $("#frmdata").on("submit", function (e) {

            e.preventDefault();

            var formulario = $(this);
            var dataSerializada = formulario.serialize();

            $.ajax({
                type: 'POST',
                url: 'index.php?cargar=editarstock_registro',
                dataType: 'json',
                data: dataSerializada

            })
                    .done(function () {

                        console.log('Correcto!');



                    })
                    .fail(function () {
                        console.log('Fallo!');
                        $("td#stock_registro_fecha" + document.getElementById('ediid').value).html(document.getElementById('edistock_registro_fecha').value);
                        $("td#prod_id" + document.getElementById('ediid').value).html(document.getElementById('ediprod_id').value);
                        $("td#n943" + document.getElementById('ediid').value).html(document.getElementById('edin943').value);
                        $("td#estacion" + document.getElementById('ediid').value).html(document.getElementById('ediestacion').value);
                        $("td#plaza" + document.getElementById('ediid').value).html(document.getElementById('ediplaza').value);
                        $("td#hj" + document.getElementById('ediid').value).html(document.getElementById('edihj').value);
                        $("td#deposito" + document.getElementById('ediid').value).html(document.getElementById('edideposito').value);
                        $("td#total" + document.getElementById('ediid').value).html(document.getElementById('editotal').value);
                        $("#myModal").modal('hide');
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
                    })







        });




    }

</script>
<script>
    $("body").on("click", ".botEliminar", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var pregunta = confirm('¿Esta seguro de eliminar esto?');
        if (pregunta == true) {
            $.ajax({
                type: 'POST',
                url: '?cargar=eliminarstock_registro&id=' + id,
                dataType: 'json'
            });
            window.location.href = 'index.php?cargar=liststock_registro';
        } else {
            return false;
        }
    })
</script>
