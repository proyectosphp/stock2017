<?php 
	$permiso=$_SESSION['permisos'];
?>

<div id="stock">
	<h1 align="center">Lista de stock</h1>

	<nav>
		<ul>
			<a class="btn btn-info"  onclick="nuevostock()">Nuevo</a>
		</ul>
	</nav> 
	<div class="input-group" style="width: 280px; margin: 0 auto;">
		<input id="filtrar" type="text" class="form-control" placeholder="Buscar...">
		<br><br>
	</div>

	<div class="table-responsive col-md-10 col-md-offset-1">
		<table class="table table-striped" border=3>
			<thead>

			<th>ID</th>
            <th>cantidad</th>
            <th>fecha</th>
            <th>producto</th>
            <th>lugar</th>

			<th>Opciones</th>
			</thead>
			<tbody id="tblRegistrosstock" class="buscar">

			</tbody>
		</table>
	</div>
</div>
<div id="nuevo">
</div>
<div id="editar">
</div>
<div id="producto">

</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content" id="div1">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<p>Some text in the modal.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

    </div>
</div>
<script>


	$(document).ready(function () {

		var lugaid = <?php echo $permiso['luga_id']; ?>;
		$.ajax({
			type: 'POST',
			url: 'routingjs.php?cargar=selectstock&luga_id='+lugaid,
			dataType: 'json',

		})
				.done(function (data) {

					console.log('Correcto!');

					console.log(data); // Se imprime en consola la api


					data.datos.forEach(function (datos) {

						var id = datos.stoc_id;
						var stoc_cant_fisi = "document.getElementById('stoc_cant_fisi" + id + "').innerHTML";
						var stoc_fech_modi = "document.getElementById('stoc_fech_modi" + id + "').innerHTML";
						var prod_id = "document.getElementById('prod_id" + id + "').innerHTML";
						var prod_nomb = "document.getElementById('stockprod_nomb" + id + "').innerHTML";
						var luga_id = "document.getElementById('luga_id" + id + "').innerHTML";
						var content = "";
						content += '<tr id="' + datos.stoc_id + '">';
						content += '<td id="stoc_id' + datos.stoc_id + '">' + datos.stoc_id + '</td>';
						content += '<td id="stoc_cant_fisi' + datos.stoc_id + '">' + datos.stoc_cant_fisi + '</td>';
						content += '<td id="stoc_fech_modi' + datos.stoc_id + '">' + datos.stoc_fech_modi + '</td>';
						content += '<td id="stockprod_nomb' + datos.stoc_id + '">' + datos.prod_nomb + '</td>';
						content += '<td style="display:none" id="prod_id' + datos.stoc_id + '">' + datos.prod_id + '</td>';
						content += '<td id="luga_id' + datos.stoc_id + '">' + datos.luga_id + '</td>';
						content += '<td>';
						content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="verstock(' + datos.stoc_id + ',' + stoc_cant_fisi + ', ' + stoc_fech_modi + ', ' + prod_nomb + ', ' + luga_id + ')">ver</a>';
						content += '<a class="btn btn-info" onclick="editarstock(' + datos.stoc_id + ',' + stoc_cant_fisi + ', ' + stoc_fech_modi + ', ' + prod_id + ', ' + prod_nomb + ', ' + luga_id + ')">editar</a>';
						content += '<a data-id="' + datos.stoc_id + '" class="btn btn-danger botEliminar" >eliminar</a>';
						content += '</td>';
						content += '</tr>';

						$("#tblRegistrosstock").append(content);
					});


				})
				.fail(function () {
					console.log("Fallo!");
					console.log(data);
				})




	});



</script>
<script>
	function verstock(stoc_id, stoc_cant_fisi, stoc_fech_modi, prod_id, luga_id) {

		$("#div1").empty();


		var content = "ver";

		content += '<form class="form" value="' + stoc_id + '" id="frmdata">';
		content += '<div class="form-group">';
		content += '<label>cantidad</label>';
		content += '<input type="number" name="stoc_cant_fisi" value="' + stoc_cant_fisi + '" class="form-control" disabled>';
		content += '</div>';
		content += '<div class="form-group">';
		content += '<label>producto</label>';
		content += '<input type="text" name="prod_id" value="' + prod_id + '" class="form-control" disabled>';
		content += '</div>';
		content += '<div class="form-group">';

		content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';
		content += '</form>';






		$("#div1").append(content);
	}


</script>
<script>
	function editarstock(stoc_id, stoc_cant_fisi, stoc_fech_modi, prod_id, prod_nomb, luga_id) {
		$("#editar").empty();


		var content = "EDITAR";

		content += '<form  value="' + stoc_id + '" id="frmdata">';
		content += '<input id="ediid" type="hidden" name="id" value="' + stoc_id + '" class="form-control">';
		content += '<div class="form-group">';
		content += '<label>cantidad</label>';
		content += '<input id="edistoc_cant_fisi" type="number" step="any" name="stoc_cant_fisi" value="' + stoc_cant_fisi + '" class="form-control">';
		content += '</div>';
		content += '<div class="form-group">';
		content += '<div class="form-group">';
		
		content += '<input type="hidden" id="prod_id" name="prod_id" value="' + prod_id + '" class="form-control">';
		content += '<label>producto</label><br>';
		content += '<input type="text" id="prod_nomb" name="prod_nomb" value="' + prod_nomb + '" class="form-control" readonly>';
		content += '</div>';

		content += '<a type="button" href="index.php?cargar=liststock" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

		content += '<button type="submit" onclick="editarrstock()" class="btn btn-success">Editar</button>';
		content += '</form>';





		$("#stock").hide();
		$("#editar").show();
		$("#editar").append(content);
	}


</script>
<script>
	function nuevostock() {

		$("#nuevo").empty();


		var content = "nuevo";

		content += '<form  id="frmdata">';
		content += '<div class="form-group">';
		content += '<label>cantidad</label>';
		content += '<input type="number" name="stoc_cant_fisi" class="form-control">';
		content += '</div>';
		content += '<div class="form-group">';
		content += '<a class="btn btn-info" onclick="producto(' + "'" + "nuevo" + "'" + ')">  Seleccionar producto</a><br>';
		content += '<input type="hidden" id="prod_id" name="prod_id" class="form-control">';
		content += '<label>producto</label><br>';
		content += '<input type="text" id="prod_nomb" name="prod_nomb" class="form-control" readonly>';
		content += '</div>';

		content += '<a type="button" href="index.php?cargar=liststock" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

		content += '<button type="submit" onclick="guardarstock()" class="btn btn-success">Crear</button>';
		content += '</form>';



		$("#stock").hide();



		$("#nuevo").append(content);
		$("#nuevo").show();
	}


</script>

<script type="text/javascript">
	$(document).ready(function () {
		(function ($) {
			$('#filtrar').keyup(function () {
				var rex = new RegExp($(this).val(), 'i');
				$('.buscar tr').hide();
				$('.buscar tr').filter(function () {
					return rex.test($(this).text());
				}).show();
			})
		}(jQuery));
	});
</script>
<script>
	function guardarstock() {

		$("#frmdata").on("submit", function (e) {

			e.preventDefault();

			var formulario = $(this);
			var dataSerializada = formulario.serialize();

			console.log(dataSerializada);

			console.log('dataSerializadddda');


			$.ajax({
				type: 'POST',
				url: 'index.php?cargar=crearstock',
				dataType: 'json',
				data: dataSerializada

			})
					.done(function (data) {

						console.log('Correcto!');

						console.log(data); // Se imprime en consola la api
						window.location.href = 'index.php?cargar=liststock';


					})
					.fail(function () {
						console.log('Fallo!');
						window.location.href = 'index.php?cargar=liststock';
					})





		});




	}

</script>
<script>
	function editarrstock() {

		$("#frmdata").on("submit", function (e) {

			e.preventDefault();
			
			var formulario = $(this);
			var dataSerializada = formulario.serialize();

			$.ajax({
				type: 'POST',
				url: 'index.php?cargar=editarstock',
				dataType: 'json',
				data: dataSerializada

			})
					.done(function () {

						console.log('Correcto!');



					})
					.fail(function () {
						console.log('Fallo!');
						$("td#stoc_cant_fisi" + document.getElementById('ediid').value).html(document.getElementById('edistoc_cant_fisi').value);
						
						$("td#prod_id" + document.getElementById('ediid').value).html(document.getElementById('prod_id').value);
						$("td#stockprod_nomb" + document.getElementById('ediid').value).html(document.getElementById('prod_nomb').value);
						
						
		$("#editar").hide();
						$("#stock").show();
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
					})







		});




	}

</script>
<script>
	$("body").on("click", ".botEliminar", function (e) {
		e.preventDefault();
		var id = $(this).data('id');
		var pregunta = confirm('¿Esta seguro de eliminar esto?');
		if (pregunta == true) {
			$.ajax({
				type: 'POST',
				url: '?cargar=eliminarstock&id=' + id,
				dataType: 'json'
			});
			window.location.href = 'index.php?cargar=liststock';
		} else {
			return false;
		}
	})
</script>
<script>
	function producto(tipo) {

		$("#producto").empty();
		if (tipo == "nuevo") {
			listaproducto_nuevo();
		} else {
			listaproducto_editar();
		}
		$("#nuevo").hide();
		$("#editar").hide();
		$("#producto").show();
		document.getElementById("producto").style.display = "";
	}
</script>

<script>  //carga los lotes por ajax mediante .load
	function listaproducto_nuevo() {

		$("#producto").load("routingjs.php?cargar=listproducto&select=nuevo");

	}
</script>

<script>  //carga los lotes por ajax mediante .load
	function listaproducto_editar() {

		$("#producto").load("routingjs.php?cargar=listproducto&select=editar");

	}
</script>
