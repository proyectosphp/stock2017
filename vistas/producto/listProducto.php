<div id="producto">
<h1 align="center">Lista de producto</h1>


<?php
  if (isset($_GET['select'])) {
      
    } else {
      echo '
            <nav>
                <ul>
                    <a class="btn btn-info" data-toggle="modal" data-target="#myModal"  onclick="nuevoproducto()">Nuevo</a>
                </ul>
            </nav>
        ';
    }

?>
<div class="input-group" style="width: 280px; margin: 0 auto;">
    <input id="filtrarproducto" type="text" class="form-control" placeholder="Buscar...">
    <br><br>
</div>

<div class="table-responsive col-md-10 col-md-offset-1">
<table class="table table-striped" border=3>
    <thead>
    
<th>ID</th>
            <th>Nombre de Producto</th>
            <th>Caracteristica</th>
            <th>Categoria</th>
            
    <th>Opciones</th>
    </thead>
    <tbody id="tblRegistrosproducto" class="buscar">
        
    </tbody>
</table>
</div>
</div>
<div id="nuevo">
</div>
<div id="editar">
</div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" id="div1">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
            <script>

    function listarproducto(){
    $(document).ready(function () {


        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectproducto',
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.prod_id;
                        var prod_nomb ="document.getElementById('prod_nomb"+id+"').innerHTML";
            var prod_cara ="document.getElementById('prod_cara"+id+"').innerHTML";
            var prod_cate_id ="document.getElementById('prod_cate_id"+id+"').innerHTML";
                var prod_cate_nomb ="document.getElementById('productoprod_cate_nomb"+id+"').innerHTML";
                    var content = "";
                        content += '<tr id="' + datos.prod_id + '">';
content += '<td id="prod_id' + datos.prod_id + '">' + datos.prod_id + '</td>';
            content += '<td id="prod_nomb' + datos.prod_id + '">' + datos.prod_nomb + '</td>';
            content += '<td id="prod_cara' + datos.prod_id + '">' + datos.prod_cara + '</td>';
            content += '<td id="productoprod_cate_nomb' + datos.prod_id + '">' + datos.prod_cate_nomb + '</td>';
                content += '<td style="display:none" id="prod_cate_id' + datos.prod_id + '">' + datos.prod_cate_id + '</td>';
                          content += '<td>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="verproducto(' + datos.prod_id + ',' + prod_nomb + ', ' + prod_cara + ', ' + prod_cate_nomb + ')">ver</a>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="editarproducto(' + datos.prod_id + ',' + prod_nomb + ', ' + prod_cara + ', ' + prod_cate_id + ')">editar</a>';
                        content += '<a data-id="' + datos.prod_id + '" class="btn btn-danger botEliminar" >eliminar</a>';
                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosproducto").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });

};

</script>
<script>

    function seleccionarproducto(tipo){
    $(document).ready(function () {


        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectproducto',
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.prod_id;
                        var prod_nomb ="document.getElementById('prod_nomb"+id+"').innerHTML";
            var prod_cara ="document.getElementById('prod_cara"+id+"').innerHTML";
            var prod_cate_id ="document.getElementById('prod_cate_id"+id+"').innerHTML";
                var prod_cate_nomb ="document.getElementById('productoprod_cate_nomb"+id+"').innerHTML";
                    var content = "";
                        content += '<tr id="' + datos.prod_id + '">';content += '<td id="prod_id' + datos.prod_id + '">' + datos.prod_id + '</td>';
            content += '<td id="prod_nomb' + datos.prod_id + '">' + datos.prod_nomb + '</td>';
            content += '<td id="prod_cara' + datos.prod_id + '">' + datos.prod_cara + '</td>';
            content += '<td id="productoprod_cate_nomb' + datos.prod_id + '">' + datos.prod_cate_nomb + '</td>';
                content += '<td style="display:none" id="prod_cate_id' + datos.prod_id + '">' + datos.prod_cate_id + '</td>';
                          content += '<td>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="ver(' + datos.prod_id + ',' + prod_nomb + ', ' + prod_cara + ', ' + prod_cate_nomb + ')">ver</a>';
                        if(tipo=='nuevo'){
                           content += '<a class="btn btn-info botseleccionar" onclick="selectproducto(' + datos.prod_id + ', ' + prod_nomb + ')">Seleccionar</a>'; 
                       }else{
                            content += '<a class="btn btn-info botseleccionar" onclick="selectproducto_editar(' + datos.prod_id + ', ' + prod_nomb + ')">Seleccionar</a>';
                       }
                        
                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosproducto").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });

};

</script>

<script>

    function seleccionarproductoDepo(tipo){
    $(document).ready(function () {


        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectproductoDepo',
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.prod_id;
                        var prod_nomb ="document.getElementById('prod_nomb"+id+"').innerHTML";
            var prod_cara ="document.getElementById('prod_cara"+id+"').innerHTML";
            var prod_cate_id ="document.getElementById('prod_cate_id"+id+"').innerHTML";
                var prod_cate_nomb ="document.getElementById('productoprod_cate_nomb"+id+"').innerHTML";
                    var content = "";
                        content += '<tr id="' + datos.prod_id + '">';content += '<td id="prod_id' + datos.prod_id + '">' + datos.prod_id + '</td>';
            content += '<td id="prod_nomb' + datos.prod_id + '">' + datos.prod_nomb + '</td>';
            content += '<td id="prod_cara' + datos.prod_id + '">' + datos.prod_cara + '</td>';
            content += '<td id="productoprod_cate_nomb' + datos.prod_id + '">' + datos.prod_cate_nomb + '</td>';
                content += '<td style="display:none" id="prod_cate_id' + datos.prod_id + '">' + datos.prod_cate_id + '</td>';
                          content += '<td>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="ver(' + datos.prod_id + ',' + prod_nomb + ', ' + prod_cara + ', ' + prod_cate_nomb + ')">ver</a>';
                        if(tipo=='nuevo'){
                           content += '<a class="btn btn-info botseleccionar" onclick="selectproducto(' + datos.prod_id + ', ' + prod_nomb + ')">Seleccionar</a>'; 
                       }else{
                            content += '<a class="btn btn-info botseleccionar" onclick="selectproducto_editar(' + datos.prod_id + ', ' + prod_nomb + ')">Seleccionar</a>';
                       }
                        
                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosproducto").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });

};

</script>


<script>
    function verproducto(prod_id, prod_nomb, prod_cara, prod_cate_id) {

        $("#div1").empty();


        var content = "ver";

        content += '<form class="form" value="' + prod_id + '" id="frmdata">';
        content += '<div class="form-group">';
            content += '<label>Nombre de Producto</label>';
            content += '<input type="text" name="prod_nomb" value="' + prod_nomb + '" class="form-control" disabled>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Caracteristica</label>';
            content += '<input type="text" name="prod_cara" value="' + prod_cara + '" class="form-control" disabled>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Categoria</label>';
            content += '<input type="text" name="prod_cate_id" value="' + prod_cate_id + '" class="form-control" disabled>';
            content += '</div>';
            
        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';
        content += '</form>';






        $("#div1").append(content);
    }


</script>
<script>
    function editarproducto(prod_id, prod_nomb, prod_cara, prod_cate_id) {
            $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectcmbproducto_categoria',
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data);

                    data.datos.forEach(function (datos) {

                        var content3 = "";
                        
                        var selected = "";
                        if (prod_cate_id==datos.prod_cate_id) {
                            selected = "selected";
                        }
    

                        
                        
                        
                        
                        content3 += '<option  ' + selected + '  value="' + datos.prod_cate_id + '">' + datos.prod_cate_nomb + '</option>';
                        $("#ediprod_cate_id").append(content3);

                    });


                })
                .fail(function () {
                    console.log('Fallo!');
                    console.log(data);
                })
        $("#div1").empty();


        var content = "EDITAR";

        content += '<form  value="' + prod_id + '" id="frmdata">';
        content += '<input id="ediid" type="hidden" name="id" value="' + prod_id + '" class="form-control">';
        content += '<div class="form-group">';
            content += '<label>Nombre de Producto</label>';
            content += '<input id="ediprod_nomb" type="text" name="prod_nomb" value="' + prod_nomb + '" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Caracteristica</label>';
            content += '<input id="ediprod_cara" type="text" name="prod_cara" value="' + prod_cara + '" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Categoria</label><br>';
            content += '<select name="prod_cate_id" id="ediprod_cate_id"></select>';
            content += '</div>';
            
        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';

        content += '<button type="submit" onclick="editarrproducto()" class="btn btn-success">Editar</button>';
        content += '</form>';






        $("#div1").append(content);
    }


</script>
<script>
    function nuevoproducto() {

    $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectcmbproducto_categoria',
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data);

                    data.datos.forEach(function (datos) {

                        var content3 = "";
    

                        
                        
                        
                        
                        content3 += '<option  value="' + datos.prod_cate_id + '">' + datos.prod_cate_nomb + '</option>';
                        $("#prod_cate_id").append(content3);

                    });


                })
                .fail(function () {
                    console.log('Fallo!');
                    console.log(data);
                })
        $("#div1").empty();


        var content = "nuevo";

        content += '<form  id="frmdata">';
        content += '<div class="form-group">';
            content += '<label>Nombre de Producto</label>';
            content += '<input type="text" name="prod_nomb" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Caracteristica</label>';
            content += '<input type="text" name="prod_cara" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Categoria</label><br>';
            content += '<select name="prod_cate_id" id="prod_cate_id"></select>';
            content += '</div>';
            
        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';

        content += '<button type="submit" onclick="guardarproducto()" class="btn btn-success">Crear</button>';
        content += '</form>';







        $("#div1").append(content);
    }


</script>

<script type="text/javascript">
    $(document).ready(function () {
        (function ($) {
            $('#filtrarproducto').keyup(function () {
                var rex = new RegExp($(this).val(), 'i');
                $('.buscar tr').hide();
                $('.buscar tr').filter(function () {
                    return rex.test($(this).text());
                }).show();
            })
        }(jQuery));
    });
</script>
<script>
function guardarproducto(){

    $("#frmdata").on("submit",function(e){

        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

        console.log(dataSerializada);
    
        console.log('dataSerializadddda');
                
                
                    $.ajax({
        type: 'POST',
        url : 'index.php?cargar=crearproducto',
        dataType: 'json',
        data:dataSerializada

    })
    .done(function( data ){
        
        console.log('Correcto!');

        console.log( data ); // Se imprime en consola la api
                window.location.href='index.php?cargar=listproducto';


    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listproducto';
    })
        
        



    });


    

}

</script>
<script>
function editarrproducto(){

    $("#frmdata").on("submit",function(e){

        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=editarproducto',
        dataType: 'json',
        data:dataSerializada

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        $("td#prod_nomb"+document.getElementById('ediid').value).html(document.getElementById('ediprod_nomb').value);
            $("td#prod_cara"+document.getElementById('ediid').value).html(document.getElementById('ediprod_cara').value);
            $("td#productoprod_cate_nomb"+document.getElementById('ediid').value).html(document.getElementById('ediprod_cate_id').options[document.getElementById('ediprod_cate_id').selectedIndex].text);
            $("td#prod_cate_id"+document.getElementById('ediid').value).html(document.getElementById('ediprod_cate_id').value);
            $("#myModal").modal('hide');
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    })
        
        

        



    });


    

}

</script>
<script>
    $("body").on("click", ".botEliminar",function(e){
    e.preventDefault();
    var id=$(this).data('id');
    var pregunta = confirm('¿Esta seguro de eliminar esto?');
    if(pregunta==true){
        $.ajax({
        type: 'POST',
        url : '?cargar=eliminarproducto&id='+id,
        dataType: 'json'
        });
        window.location.href='index.php?cargar=listproducto';
    }else{
        return false;
    }
})
</script>

<script>
 
 function  selectproducto(id, prod_nomb){
    document.getElementById('prod_id').value=id;
    document.getElementById('prod_nomb').value=prod_nomb;
    $("#producto").hide();
    $("#nuevo").show();
}

</script>
<script>
 
 function  selectproducto_editar(id, prod_nomb){
    document.getElementById('prod_id').value=id;
    document.getElementById('prod_nomb').value=prod_nomb;
    $("#producto").hide();
    $("#producto").empty();
    $("#editar").show();
}

</script>

<?php
    if(isset($_GET['depo'])){
        if (isset($_GET["select"])) {
        if($_GET["select"]=='nuevo'){
            echo "  <script>seleccionarproductoDepo('nuevo') </script>    ";
        }elseif($_GET["select"]=='editar'){
            echo "  <script>seleccionarproductoDepo('editar') </script>    ";
        }
      
    } else {
      echo '  <script>listarproducto() </script>    '; 
    }
    }else{
        if (isset($_GET["select"])) {
        if($_GET["select"]=='nuevo'){
            echo "  <script>seleccionarproducto('nuevo') </script>    ";
        }elseif($_GET["select"]=='editar'){
            echo "  <script>seleccionarproducto('editar') </script>    ";
        }
      
    } else {
      echo '  <script>listarproducto() </script>    '; 
    }
    }
    

?>