<?php
if (isset($_SESSION['logeo'])){
        $form=$_SESSION['logeo'];
    }
    if(!isset($form)){
        echo "<SCRIPT LANGUAGE=JavaScript> 
                window.location.href='index.php';
            </SCRIPT>";
    }
$permisos=$_SESSION['permisos'];
if($permisos['tipo_perm_nomb']=='admin'){
?>
<div id="usuario">
<h1 align="center">Lista de usuario</h1>


<?php

  if (isset($_GET['select'])) {
      
    } else {
      echo '
            <nav>
                <ul>
                    <a class="btn btn-info" data-toggle="modal" data-target="#myModal"  onclick="nuevousuario()">Nuevo</a>
                </ul>
            </nav>
        ';
    }

?>
<div class="input-group" style="width: 280px; margin: 0 auto;">
    <input id="filtrar" type="text" class="form-control" placeholder="Buscar...">
    <br><br>
</div>

<div class="table-responsive col-md-10 col-md-offset-1">
<table class="table table-striped" border=3>
    <thead>
    
<th>ID</th>
            <th>Nombre de usuario</th>
            <th>Contraseña</th>
            
    <th>Opciones</th>
    </thead>
    <tbody id="tblRegistrosusuario" class="buscar">
        
    </tbody>
</table>
</div>
</div>
<div id="nuevo">
</div>
<div id="editar">
</div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" id="div1">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
            <script>

    function listarusuario(){
    $(document).ready(function () {


        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectusuario',
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.usua_id;
                        var usua_nomb ="document.getElementById('usua_nomb"+id+"').innerHTML";
            var usua_pass ="document.getElementById('usua_pass"+id+"').innerHTML";
                    var content = "";
                        content += '<tr id="' + datos.usua_id + '">';
content += '<td id="usua_id' + datos.usua_id + '">' + datos.usua_id + '</td>';
            content += '<td id="usua_nomb' + datos.usua_id + '">' + datos.usua_nomb + '</td>';
            content += '<td id="usua_pass' + datos.usua_id + '">' + datos.usua_pass + '</td>';
                          content += '<td>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="verusuario(' + datos.usua_id + ',' + usua_nomb + ', ' + usua_pass + ')">ver</a>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="editarusuario(' + datos.usua_id + ',' + usua_nomb + ', ' + usua_pass + ')">editar</a>';
                        content += '<a data-id="' + datos.usua_id + '" class="btn btn-danger botEliminar" >eliminar</a>';
                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosusuario").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });

};

</script>
<script>

    function seleccionarusuario(tipo){
    $(document).ready(function () {


        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectusuario',
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.usua_id;
                        var usua_nomb ="document.getElementById('usua_nomb"+id+"').innerHTML";
            var usua_pass ="document.getElementById('usua_pass"+id+"').innerHTML";
                    var content = "";
                        content += '<tr id="' + datos.usua_id + '">';content += '<td id="usua_id' + datos.usua_id + '">' + datos.usua_id + '</td>';
            content += '<td id="usua_nomb' + datos.usua_id + '">' + datos.usua_nomb + '</td>';
            content += '<td id="usua_pass' + datos.usua_id + '">' + datos.usua_pass + '</td>';
                          content += '<td>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="ver(' + datos.usua_id + ',' + usua_nomb + ', ' + usua_pass + ')">ver</a>';
                        if(tipo=='nuevo'){
                           content += '<a class="btn btn-info botseleccionar" onclick="selectusuario(' + datos.usua_id + ', ' + usua_nomb + ')">Seleccionar</a>'; 
                       }else{
                            content += '<a class="btn btn-info botseleccionar" onclick="selectusuario_editar(' + datos.usua_id + ', ' + usua_nomb + ')">Seleccionar</a>';
                       }
                        
                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosusuario").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });

};

</script>
<script>
    function verusuario(usua_id, usua_nomb, usua_pass) {

        $("#div1").empty();


        var content = "ver";

        content += '<form class="form" value="' + usua_id + '" id="frmdata">';
        content += '<div class="form-group">';
            content += '<label>Nombre de usuario</label>';
            content += '<input type="text" name="usua_nomb" value="' + usua_nomb + '" class="form-control" disabled>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Contraseña</label>';
            content += '<input type="text" name="usua_pass" value="' + usua_pass + '" class="form-control" disabled>';
            content += '</div>';
            
        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';
        content += '</form>';






        $("#div1").append(content);
    }


</script>
<script>
    function editarusuario(usua_id, usua_nomb, usua_pass) {
            $("#div1").empty();


        var content = "EDITAR";

        content += '<form  value="' + usua_id + '" id="frmdata">';
        content += '<input id="ediid" type="hidden" name="id" value="' + usua_id + '" class="form-control">';
        content += '<div class="form-group">';
            content += '<label>Nombre de usuario</label>';
            content += '<input id="ediusua_nomb" type="text" name="usua_nomb" value="' + usua_nomb + '" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Contraseña</label>';
            content += '<input id="ediusua_pass" type="text" name="usua_pass" value="' + usua_pass + '" class="form-control" required>';
            content += '</div>';
            
        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';

        content += '<button type="submit" onclick="editarrusuario()" class="btn btn-success">Editar</button>';
        content += '</form>';






        $("#div1").append(content);
    }


</script>
<script>
    function nuevousuario() {

    $("#div1").empty();


        var content = "nuevo";

        content += '<form  id="frmdata">';
        content += '<div class="form-group">';
            content += '<label>Nombre de usuario</label>';
            content += '<input type="text" name="usua_nomb" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<label>Contraseña</label>';
            content += '<input type="text" name="usua_pass" class="form-control" required>';
            content += '</div>';
            
        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';

        content += '<button type="submit" onclick="guardarusuario()" class="btn btn-success">Crear</button>';
        content += '</form>';







        $("#div1").append(content);
    }


</script>

<script type="text/javascript">
    $(document).ready(function () {
        (function ($) {
            $('#filtrar').keyup(function () {
                var rex = new RegExp($(this).val(), 'i');
                $('.buscar tr').hide();
                $('.buscar tr').filter(function () {
                    return rex.test($(this).text());
                }).show();
            })
        }(jQuery));
    });
</script>
<script>
function guardarusuario(){

    $("#frmdata").on("submit",function(e){

        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

        console.log(dataSerializada);
    
        console.log('dataSerializadddda');
                
                
                    $.ajax({
        type: 'POST',
        url : 'index.php?cargar=crearusuario',
        dataType: 'json',
        data:dataSerializada

    })
    .done(function( data ){
        
        console.log('Correcto!');

        console.log( data ); // Se imprime en consola la api
                window.location.href='index.php?cargar=listusuario';


    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listusuario';
    })
        
        



    });


    

}

</script>
<script>
function editarrusuario(){

    $("#frmdata").on("submit",function(e){

        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=editarusuario',
        dataType: 'json',
        data:dataSerializada

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        $("td#usua_nomb"+document.getElementById('ediid').value).html(document.getElementById('ediusua_nomb').value);
            $("td#usua_pass"+document.getElementById('ediid').value).html(document.getElementById('ediusua_pass').value);
            $("#myModal").modal('hide');
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    })
        
        

        



    });


    

}

</script>
<script>
    $("body").on("click", ".botEliminar",function(e){
    e.preventDefault();
    var id=$(this).data('id');
    var pregunta = confirm('¿Esta seguro de eliminar esto?');
    if(pregunta==true){
        $.ajax({
        type: 'POST',
        url : '?cargar=eliminarusuario&id='+id,
        dataType: 'json'
        });
        window.location.href='index.php?cargar=listusuario';
    }else{
        return false;
    }
})
</script>

<script>
 
 function  selectusuario(id, usua_nomb){
    document.getElementById('usua_id').value=id;
    document.getElementById('usua_nomb').value=usua_nomb;
    $("#usuario").hide();
    $("#nuevo").show();
}

</script>
<script>
 
 function  selectusuario_editar(id, usua_nomb){
    document.getElementById('usua_id').value=id;
    document.getElementById('usua_nomb').value=usua_nomb;
    $("#usuario").hide();
    $("#usuario").empty();
    $("#editar").show();
}

</script>

<?php
    if (isset($_GET["select"])) {
        if($_GET["select"]=='nuevo'){
            echo "  <script>seleccionarusuario('nuevo') </script>    ";
        }elseif($_GET["select"]=='editar'){
            echo "  <script>seleccionarusuario('editar') </script>    ";
        }
      
    } else {
      echo '  <script>listarusuario() </script>    '; 
    }
}else{
        echo "<SCRIPT LANGUAGE=JavaScript> 
                window.location.href='index.php';
            </SCRIPT>";
    }
?>