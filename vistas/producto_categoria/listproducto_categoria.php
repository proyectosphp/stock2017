<?php
if (isset($_SESSION['logeo'])){
        $form=$_SESSION['logeo'];
    }
    if(!isset($form)){
        echo "<SCRIPT LANGUAGE=JavaScript> 
                window.location.href='index.php';
            </SCRIPT>";
    }
$permisos=$_SESSION['permisos'];
if($permisos['tipo_perm_nomb']=='admin'){
?>
<div id="producto_categoria">
<h1 align="center">Lista de producto_categoria</h1>

<nav>
            <ul>
                <a class="btn btn-info" data-toggle="modal" data-target="#myModal"  onclick="nuevoproducto_categoria()">Nuevo</a>
            </ul>
</nav>
<div class="input-group" style="width: 280px; margin: 0 auto;">
    <input id="filtrar" type="text" class="form-control" placeholder="Buscar...">
    <br><br>
</div>

<div class="table-responsive col-md-10 col-md-offset-1">
<table class="table table-striped" border=3>
    <thead>
    
<th>ID</th>
            <th>Nombre de Categoria</th>
            
    <th>Opciones</th>
    </thead>
    <tbody id="tblRegistrosproducto_categoria" class="buscar">
        
    </tbody>
</table>
</div>
</div>
<div id="nuevo">
</div>
<div id="editar">
</div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" id="div1">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
            <script>


    $(document).ready(function () {


        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectproducto_categoria',
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.prod_cate_id;
                        var prod_cate_nomb ="document.getElementById('prod_cate_nomb"+id+"').innerHTML";
                    var content = "";
                        content += '<tr id="' + datos.prod_cate_id + '">';
content += '<td id="prod_cate_id' + datos.prod_cate_id + '">' + datos.prod_cate_id + '</td>';
            content += '<td id="prod_cate_nomb' + datos.prod_cate_id + '">' + datos.prod_cate_nomb + '</td>';
                          content += '<td>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="verproducto_categoria(' + datos.prod_cate_id + ',' + prod_cate_nomb + ')">ver</a>';
                        content += '<a class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="editarproducto_categoria(' + datos.prod_cate_id + ',' + prod_cate_nomb + ')">editar</a>';
                        content += '<a data-id="' + datos.prod_cate_id + '" class="btn btn-danger botEliminar" >eliminar</a>';
                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosproducto_categoria").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });



</script>
<script>
    function verproducto_categoria(prod_cate_id, prod_cate_nomb) {

        $("#div1").empty();


        var content = "ver";

        content += '<form class="form" value="' + prod_cate_id + '" id="frmdata">';
        content += '<div class="form-group">';
            content += '<label>Nombre de Categoria</label>';
            content += '<input type="text" name="prod_cate_nomb" value="' + prod_cate_nomb + '" class="form-control" disabled>';
            content += '</div>';
            
        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';
        content += '</form>';






        $("#div1").append(content);
    }


</script>
<script>
    function editarproducto_categoria(prod_cate_id, prod_cate_nomb) {
            $("#div1").empty();


        var content = "EDITAR";

        content += '<form  value="' + prod_cate_id + '" id="frmdata">';
        content += '<input id="ediid" type="hidden" name="id" value="' + prod_cate_id + '" class="form-control">';
        content += '<div class="form-group">';
            content += '<label>Nombre de Categoria</label>';
            content += '<input id="ediprod_cate_nomb" type="text" name="prod_cate_nomb" value="' + prod_cate_nomb + '" class="form-control" required>';
            content += '</div>';
            
        content += '<a type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

        content += '<button type="submit" onclick="editarrproducto_categoria()" class="btn btn-success">Editar</button>';
        content += '</form>';






        $("#div1").append(content);
    }


</script>
<script>
    function nuevoproducto_categoria() {

    $("#div1").empty();


        var content = "nuevo";

        content += '<form  id="frmdata">';
        content += '<div class="form-group">';
            content += '<label>Nombre de Categoria</label>';
            content += '<input type="text" name="prod_cate_nomb" class="form-control" required>';
            content += '</div>';
            
        content += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>';

        content += '<button type="submit" onclick="guardarproducto_categoria()" class="btn btn-success">Crear</button>';
        content += '</form>';







        $("#div1").append(content);
    }


</script>

<script type="text/javascript">
    $(document).ready(function () {
        (function ($) {
            $('#filtrar').keyup(function () {
                var rex = new RegExp($(this).val(), 'i');
                $('.buscar tr').hide();
                $('.buscar tr').filter(function () {
                    return rex.test($(this).text());
                }).show();
            })
        }(jQuery));
    });
</script>
<script>
function guardarproducto_categoria(){

    $("#frmdata").on("submit",function(e){

        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

        console.log(dataSerializada);
    
        console.log('dataSerializadddda');
                
                
                    $.ajax({
        type: 'POST',
        url : 'index.php?cargar=crearproducto_categoria',
        dataType: 'json',
        data:dataSerializada

    })
    .done(function( data ){
        
        console.log('Correcto!');

        console.log( data ); // Se imprime en consola la api
                window.location.href='index.php?cargar=listproducto_categoria';


    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listproducto_categoria';
    })
        
        



    });


    

}

</script>
<script>
function editarrproducto_categoria(){

    $("#frmdata").on("submit",function(e){

        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=editarproducto_categoria',
        dataType: 'json',
        data:dataSerializada

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        $("td#prod_cate_nomb"+document.getElementById('ediid').value).html(document.getElementById('ediprod_cate_nomb').value);
            $("#myModal").modal('hide');
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    })
        
        

        



    });


    

}

</script>
<script>
    $("body").on("click", ".botEliminar",function(e){
    e.preventDefault();
    var id=$(this).data('id');
    var pregunta = confirm('¿Esta seguro de eliminar esto?');
    if(pregunta==true){
        $.ajax({
        type: 'POST',
        url : '?cargar=eliminarproducto_categoria&id='+id,
        dataType: 'json'
        });
        window.location.href='index.php?cargar=listproducto_categoria';
    }else{
        return false;
    }
})
</script>
<?php
}else{
        echo "<SCRIPT LANGUAGE=JavaScript> 
                window.location.href='index.php';
            </SCRIPT>";
    }
?>
