<?php
    if (isset($_SESSION['logeo'])){
        $form=$_SESSION['logeo'];
    }
    if(!isset($form)){
        echo "<SCRIPT LANGUAGE=JavaScript> 
                window.location.href='index.php';
            </SCRIPT>";
    }
    $permiso=$_SESSION['permisos'];
    $controlador = new pedidoControlador();
    $row=$controlador->ver($_GET['pedi_id']);
?>
<div id="detalle_pedido">
<h1 align="center">Lista de Productos del Pedido</h1>

<nav>
            <ul>
            <?php
               if($permiso['luga_depo']==0){
                    if($row['esta_pedi_nomb']=="En Proceso"){
                        echo '<a class="btn btn-info"  onclick="nuevodetalle_pedido()">Nuevo</a>';
                        echo '<a class="btn btn-info"  onclick="editarrealizado()">OK</a>';
                    }elseif($row['esta_pedi_nomb']=="Aceptado"){
                        echo '<a class="btn btn-info"  onclick="editarrecibido(dataprod)">Recibido</a>';
                    }
               }else{
                    if($row['esta_pedi_nomb']=="Realizado"){
						
                        echo '<a id="btnaceptar" class="btn btn-info"  onclick="editaraceptado()">Aceptar</a>';
                        echo '<a class="btn btn-info"  onclick="editarrechazado()">Rechazar</a>';
                    }
               } 
            ?>
                
                <a class="btn btn-info"  href="index.php?cargar=listpedido">Volver</a>
            </ul>
</nav>
<div class="input-group" style="width: 280px; margin: 0 auto;">
    <input id="filtrar" type="text" class="form-control" placeholder="Buscar...">
    <br><br>
</div>

<div class="table-responsive col-md-10 col-md-offset-1">
<table class="table table-striped" border=3>
    <thead>
    
<th style="display:none">ID</th>
            <?php 
                if($permiso['luga_depo']==0){
                    if($row['esta_pedi_nomb']=="Recibido"){
                        echo '<th>Cantidad Recibida</th>';
                    }elseif($row['esta_pedi_nomb']=="Aceptado"){
                        echo '<th>Cantidad</th>';
                    }else{
                        echo '<th>Cantidad Pedida</th>';
                    }
                }else{
                    echo '<th>Cantidad Pedida</th>';
                    echo '<th>Cantidad Aceptada</th>';
                    if($row['esta_pedi_nomb']=="Realizado"){
                        echo '<th>Cantidad Stock Actual</th>';
                    }
                }
            ?>
            <th>Producto</th>
            
    <th>Opciones</th>
    </thead>
    <tbody id="tblRegistrosdetalle_pedido" class="buscar">
        
    </tbody>
</table>
</div>
</div>
<div id="nuevo">
</div>
<div id="editar">
</div>
<div id="producto">

</div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" id="div1">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
            <script>


    $(document).ready(function () {
        var idpedi = <?php echo $_GET['pedi_id']; ?>;
        var lugadepo = <?php echo $permiso['luga_depo']; ?>;
        var lugaid = <?php echo $permiso['luga_id']; ?>;
        var estadopedi = "<?php echo $row['esta_pedi_nomb']; ?>";
        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectdetalle_pedido&pedi_id='+idpedi+'&luga_id='+lugaid,
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {

                        var id = datos.deta_pedi_id;
                        var deta_pedi_cant_pedi ="document.getElementById('deta_pedi_cant_pedi"+id+"').innerHTML";
            var deta_pedi_cant_acep ="document.getElementById('deta_pedi_cant_acep"+id+"').innerHTML";
            var prod_id ="document.getElementById('prod_id"+id+"').innerHTML";
                var prod_nomb ="document.getElementById('detalle_pedidoprod_nomb"+id+"').innerHTML";
                    var content = "";
                        content += '<tr id="' + datos.deta_pedi_id + '"';
                        if(estadopedi=="Realizado"){
                           if (Math.abs(datos.deta_pedi_cant_acep) > Math.abs(datos.deposito_stock) ) {
                            content += ' style="border: 5px solid red"';
                            document.getElementById('btnaceptar').style.display = 'none';
                         } 
                        }
						content += '>';
						
content += '<td id="deta_pedi_id' + datos.deta_pedi_id + '" style="display:none">' + datos.deta_pedi_id + '</td>';
            if(lugadepo==0){
                if(estadopedi=="Recibido"){
                    content += '<td id="deta_pedi_cant_acep' + datos.deta_pedi_id + '">' + datos.deta_pedi_cant_acep + '</td>';
                }else if(estadopedi=="Aceptado"){
                    content += '<td id="deta_pedi_cant_acep' + datos.deta_pedi_id + '">' + datos.deta_pedi_cant_acep + '</td>';
                }else{
                    content += '<td id="deta_pedi_cant_pedi' + datos.deta_pedi_id + '">' + datos.deta_pedi_cant_pedi + '</td>';
                }
            }else{
                content += '<td id="deta_pedi_cant_pedi' + datos.deta_pedi_id + '">' + datos.deta_pedi_cant_pedi + '</td>';
                content += '<td id="deta_pedi_cant_acep' + datos.deta_pedi_id + '">' + datos.deta_pedi_cant_acep + '</td>';
                if(estadopedi=="Realizado"){
                    content += '<td id="stoc_cant_fisi' + datos.deta_pedi_id + '">' + datos.deposito_stock + '</td>';
                }
            }
            content += '<td id="detalle_pedidoprod_nomb' + datos.deta_pedi_id + '">' + datos.prod_nomb + '</td>';
                content += '<td style="display:none" id="prod_id' + datos.deta_pedi_id + '">' + datos.prod_id + '</td>';
                          content += '<td>';
                          if(lugadepo==0){
                            if(estadopedi=="En Proceso"){
                                content += '<a class="btn btn-info" onclick="editardetalle_pedido(' + datos.deta_pedi_id + ',' + deta_pedi_cant_pedi + ', ' + prod_id + ', ' + prod_nomb + ')">editar</a>';
                                content += '<a data-id="' + datos.deta_pedi_id + '" class="btn btn-danger botEliminar" >eliminar</a>';
                            }
                          }else{
                            if(estadopedi=="Realizado"){
								
									
											content += '<a class="btn btn-info" onclick="editardetalle_pedidodepo(' + datos.deta_pedi_id + ', ' + deta_pedi_cant_acep + ', ' + deta_pedi_cant_pedi + ', ' + prod_id + ')">editar</a>';
                                           
                                
                            }
                          }
                        content += '</td>';
                        content += '</tr>';

                        $("#tblRegistrosdetalle_pedido").append(content);
                    });


                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });



</script>
<script>
    function editardetalle_pedido(deta_pedi_id, deta_pedi_cant_pedi, prod_id, prod_nomb) {
            $("#editar").empty();

            var idpedi = <?php echo $_GET['pedi_id']; ?>;
        var content = "EDITAR";

        content += '<form  value="' + deta_pedi_id + '" id="frmdata">';
        content += '<input id="ediid" type="hidden" name="id" value="' + deta_pedi_id + '" class="form-control">';
        content += '<div class="form-group">';
            content += '<label>Cantidad Pedida</label>';
            content += '<input id="edideta_pedi_cant_pedi" min="0" type="number" name="deta_pedi_cant_pedi" value="' + deta_pedi_cant_pedi + '" class="form-control">';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<a class="btn btn-info" onclick="producto('+"'"+"editar"+"'"+')">  Seleccionar Producto</a><br>';
            content += '<input type="hidden" id="prod_id" name="prod_id" value="' + prod_id + '" class="form-control">';
            content += '<label>Producto</label><br>';
            content += '<input type="text" id="prod_nomb" name="prod_nomb" value="' + prod_nomb + '" class="form-control" readonly>';
            content += '</div>';
            
        content += '<a type="button" href="index.php?cargar=listdetalle_pedido&pedi_id='+idpedi+'" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

        content += '<button type="submit" onclick="editarrdetalle_pedido()" class="btn btn-success">Editar</button>';
        content += '</form>';





        $("#detalle_pedido").hide();
        $("#editar").show();
        $("#editar").append(content);
    }


</script>
<script>
    function editardetalle_pedidodepo(deta_pedi_id, deta_pedi_cant_acep, deta_pedi_cant_pedi, prod_id) {
            $("#editar").empty();
            var idpedi = <?php echo $_GET['pedi_id']; ?>;
        var content = "EDITAR";

        content += '<form  value="' + deta_pedi_id + '" id="frmdata">';
        content += '<input id="ediid" type="hidden" name="id" value="' + deta_pedi_id + '" class="form-control">';
        content += '<div class="form-group">';
            content += '<label>Cantidad Aceptada</label>';
            content += '<input id="edideta_pedi_cant_acep" min="0" max="'+deta_pedi_cant_pedi+'" type="number" name="deta_pedi_cant_acep" value="' + deta_pedi_cant_acep + '" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<input type="hidden" id="prod_id" name="prod_id" value="' + prod_id + '" class="form-control">';
            content += '</div>';
            
        content += '<a type="button" href="index.php?cargar=listdetalle_pedido&pedi_id='+idpedi+'" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

        content += '<button type="submit" onclick="editarrdetalle_pedido()" class="btn btn-success">Editar</button>';
        content += '</form>';





        $("#detalle_pedido").hide();
        $("#editar").show();
        $("#editar").append(content);
    }


</script>
<script>
    function nuevodetalle_pedido() {

    $("#nuevo").empty();

    var idpedi = <?php echo $_GET['pedi_id']; ?>;
        var content = "nuevo";

        content += '<form  id="frmdata">';
        content += '<div class="form-group">';
            content += '<label>Cantidad Pedida</label>';
            content += '<input type="number" min="0" name="deta_pedi_cant_pedi" class="form-control" required>';
            content += '</div>';
            content += '<div class="form-group">';
            content += '<a class="btn btn-info" onclick="producto('+"'"+"nuevo"+"'"+')">  Seleccionar Producto</a><br>';
            content += '<input type="hidden" id="prod_id" name="prod_id" class="form-control" required>';
            content += '<label>Producto</label><br>';
            content += '<input type="text" id="prod_nomb" name="prod_nomb" class="form-control" readonly>';
            content += '</div>';
            
        content += '<a type="button" href="index.php?cargar=listdetalle_pedido&pedi_id='+idpedi+'" class="btn btn-warning" data-dismiss="modal">Cerrar</a>';

        content += '<button type="submit" onclick="guardardetalle_pedido()" class="btn btn-success">Crear</button>';
        content += '</form>';



        $("#detalle_pedido").hide();



        $("#nuevo").append(content);
        $("#nuevo").show();
    }


</script>

<script type="text/javascript">
    $(document).ready(function () {
        (function ($) {
            $('#filtrar').keyup(function () {
                var rex = new RegExp($(this).val(), 'i');
                $('.buscar tr').hide();
                $('.buscar tr').filter(function () {
                    return rex.test($(this).text());
                }).show();
            })
        }(jQuery));
    });
</script>
<script>
function guardardetalle_pedido(){

    $("#frmdata").on("submit",function(e){
        var idpedi = <?php echo $_GET['pedi_id']; ?>;
        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

        console.log(dataSerializada);
    
        console.log('dataSerializadddda');
                
                
                    $.ajax({
        type: 'POST',
        url : 'index.php?cargar=creardetalle_pedido&pedi_id='+idpedi,
        dataType: 'json',
        data:dataSerializada

    })
    .done(function( data ){
        
        console.log('Correcto!');

        console.log( data ); // Se imprime en consola la api
                window.location.href='index.php?cargar=listdetalle_pedido&pedi_id='+idpedi;


    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listdetalle_pedido&pedi_id='+idpedi;
    })
        
        



    });


    

}

</script>
<script>
function editarrdetalle_pedido(){

    $("#frmdata").on("submit",function(e){
        var idpedi = <?php echo $_GET['pedi_id']; ?>;
        e.preventDefault();

        var formulario = $(this);
        var dataSerializada = formulario.serialize();

           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=editardetalle_pedido',
        dataType: 'json',
        data:dataSerializada

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listdetalle_pedido&pedi_id='+idpedi;
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    })
        
        

        



    });


    

}

</script>

<script>
function editarrealizado(){
        var idpedi = <?php echo $_GET['pedi_id']; ?>;
           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=procesos&proceso=Realizado&pedi_id='+idpedi,
        dataType: 'json'

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listpedido';
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    });


    

}

</script>

<script>
function editaraceptado(){


        var idpedi = <?php echo $_GET['pedi_id']; ?>;
           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=procesos&proceso=Aceptado&pedi_id='+idpedi,
        dataType: 'json'

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listpedido';
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    });


    

}

</script>

<script>
function editarrechazado(){
        var idpedi = <?php echo $_GET['pedi_id']; ?>;
           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=procesos&proceso=Rechazado&pedi_id='+idpedi,
        dataType: 'json'

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listpedido';
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    });


    

}

</script>

<script>
dataprod = {datosprod: []};    
    
     n=0;

    $(document).ready(function () {
        var idpedi = <?php echo $_GET['pedi_id']; ?>;
        var luga = <?php echo $permiso['luga_id']; ?>;
        $.ajax({
            type: 'POST',
            url: 'routingjs.php?cargar=selectdetalle_pedido&pedi_id='+idpedi+'&luga_id='+luga,
            dataType: 'json',

        })
                .done(function (data) {

                    console.log('Correcto!');

                    console.log(data); // Se imprime en consola la api


                    data.datos.forEach(function (datos) {
                        n=dataprod.datosprod.length; 
        
        
                        cant=datos.deta_pedi_cant_acep;
                        prodid=datos.prod_id;
                        dataprod.datosprod[n]={0:n,1:cant,2:prodid,3:luga};
                        
                        
                    });
                    console.log(dataprod);
                })
                .fail(function () {
                    console.log("Fallo!");
                    console.log(data);
                })




    });



</script>


<script>
function editarrecibido(dataprod){
        var idpedi = <?php echo $_GET['pedi_id']; ?>;
        var r = confirm('Se sumaran los productos al stock de este lugar, ¿Desea aceptar?');
        if(r==true){

           $.ajax({
        type: 'POST',
        url : 'index.php?cargar=procesos&proceso=Recibido&pedi_id='+idpedi,
        dataType: 'json',
        data:dataprod

    })
    .done(function(){
        
        console.log('Correcto!');
       
           

    })
    .fail(function(){
        console.log('Fallo!');
        window.location.href='index.php?cargar=listpedido';
//       var registro = data.datos[0];
//       console.log("asdfasdfasdf"+registro);
    });


    }

}

</script>
<script>
    $("body").on("click", ".botEliminar",function(e){
    e.preventDefault();
    var id=$(this).data('id');
    var pregunta = confirm('¿Esta seguro de eliminar esto?');
    if(pregunta==true){
        $.ajax({
        type: 'POST',
        url : '?cargar=eliminardetalle_pedido&id='+id,
        dataType: 'json'
        });
        window.location.href='index.php?cargar=listdetalle_pedido';
    }else{
        return false;
    }
})
</script>
<script>
    function producto(tipo) {

        $("#producto").empty();
        if(tipo=="nuevo"){
        listaproducto_nuevo();
        }else{
          listaproducto_editar();  
        }
        $("#nuevo").hide();
        $("#editar").hide();
        $("#producto").show();
        document.getElementById("producto").style.display = "";
    }
</script>

<script>  //carga los lotes por ajax mediante .load
    function listaproducto_nuevo() {

        $("#producto").load("routingjs.php?cargar=listproducto&select=nuevo&depo=depo");

    }
</script>

<script>  //carga los lotes por ajax mediante .load
    function listaproducto_editar() {

        $("#producto").load("routingjs.php?cargar=listproducto&select=editar&depo=depo");

    }
</script>
            