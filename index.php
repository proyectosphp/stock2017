<?php

include_once("modulos/enrutador.php");
include_once("modulos/usuario-Controlador.php");
include_once("modulos/controladorLugar.php");
include_once("modulos/producto_categoria-Controlador.php");
include_once("modulos/producto-Controlador.php");
include_once("modulos/controladorPermiso.php");
include_once("modulos/controladorInformacion.php");
include_once("modulos/pedido-Controlador.php");
include_once("modulos/ingreso-Controlador.php");
include_once("modulos/detalle_ingreso-Controlador.php");
include_once("modulos/detalle_pedido-Controlador.php");

include_once("modulos/controladorPedidoStockIngreso.php");
include_once("modulos/stock_registro-Controlador.php");
include_once("modulos/stock-Controlador.php");



//include_once("modulos/stock_registro-Controlador.php");

    session_start();     
    if (isset($_SESSION['logeo'])){
        $form=$_SESSION['logeo'];
    }
        
    if (isset($_SESSION['permisos'])){
        $permisos=$_SESSION['permisos'];
    }

?>

<! DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sistema de Control de Stock</title>
        <link rel="stylesheet" href="css/estilos.css">
        <script src="js/jquery-3.1.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
<!--        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
    </head>
    <body>
        <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"  aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" >Stock y Pedidos</a>
        </div>
       
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
        <?php
         if(isset($form))
            {
             if(isset($permisos)){
             echo '<li class="active"><a href="index.php?cargar=borrarVariablePermisos">Inicio</a></li>';}
            }  else {
              
              echo '<li class="active"><a href="index.php">Inicio</a></li>';
            }

        
        
        if (isset($permisos['luga_depo']) and isset($permisos['tipo_perm_nomb']))
        {
       
            if ($permisos['luga_depo']==0 and $permisos['tipo_perm_nomb']=='encargado') {
                echo '<li><a href="?cargar=listpedido">pedidos</a></li>';
                echo '<li><a href="?cargar=liststock">stock</a></li>';
            } 

            if ($permisos['luga_depo']==1 and $permisos['tipo_perm_nomb']=='encargado') {
                echo '<li><a href="?cargar=listingreso">ingresos</a></li>';

                echo '<li><a href="?cargar=liststock">stock</a></li>';
                echo '<li><a href="?cargar=listpedido">pedidos</a></li>';

                
            } 

            if ($permisos['tipo_perm_nomb']=='admin') {
                echo '<li><a href="?cargar=listLugar">lugar</a></li>';
                echo '<li><a href="?cargar=listproducto_categoria">categoria</a></li>';
                echo '<li><a href="?cargar=listusuario">usuarios</a></li>';
                echo '<li><a href="?cargar=listproducto">producto</a></li>';
                echo '<li><a href="?cargar=listPermiso">permisos</a></li>';
                echo '<li><a href="?cargar=informacion">informacion</a></li>';
            }
         }
        ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">
        
        <?php if(isset($form))
            { 
              echo '<li><a href="?cargar=logout">LogOut</a></li>'; 
            }  else {
              
              echo '<li><a href="?cargar=login">LogIn</a></li>';
            }
        ?>

          </ul>
        </div>
        </div>
        
        <h1> <?php if(isset($form['id'])){ echo 'Bienvenido '.$form['nombre'].'.'; if(isset($permisos['luga_nomb'])){ echo 'Estas en '.$permisos['luga_nomb'];}}?></h1>
    </body>
    <section>
        <div class="col-md-10 col-md-offset-1">
        <?php
            $enrutador = new enrutador();
            
           
                 if(isset($_GET['cargar'])){ 
                 $enrutador->cargarVista($_GET['cargar']);
                 }  else {
                   include_once('index.php');
                   
                 }
            
               
            
            
           
        ?>   
        </div>
    </section>
</html>




</body> 

